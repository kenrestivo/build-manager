#!/bin/sh

# Compiles the mermaid file into an SVG diagram artifact.
# Run from the top level of the repository.
# Give it an arg and it will push it to s3.

docker run -it -v `pwd`:/tmp arnau/mermaid mermaid -s -o /tmp /tmp/flow.mermaid

if [ "$1" ]; then
    aws s3 cp flow.mermaid.svg s3://brownbag-static/pipeline-build-manager.svg --acl public-read
fi
