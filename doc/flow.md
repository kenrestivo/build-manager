# Flow using the build manager

![alt text](https://brownbag-static.s3.amazonaws.com/pipeline-build-manager.svg "Typical flow")

[Link here in case it isn't legible on your screen](https://brownbag-static.s3.amazonaws.com/pipeline-build-manager.svg)

This is generated from a mermaid format file https://knsv.github.io/mermaid/#mermaid

