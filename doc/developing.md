# Notes on developing


## Adding a new field
From time to time you might need to add a new field to build-manager. Here's how:

- Add a new migration to schema.edn to add the field to the db. Migrations run automatically at run time, so restart to run it.
- Add the field to model.clj, defschema (take note of optional-key if needed, and value-xforms
- Add the field to the spec in specs.clj if required there (only for generative tests)
- Add to the nested-foo structures in org.restivo.build-manager.db.queries ! This will assure it'll get returned in calls.
- Add to queries_test.clj
- Add the field to the test data in resources/test-data and to the tests in api_test.clj


## Adding a new endpoint
From time to time, you might need to add a new endpoint. There's a bit too much bureaucracy/ceremony involved for my personal taste, but here's the current process:
- If you're also adding new fields/entities, see (and do) above first.
- If it's a report that requires a query, add the query to src/org/restivo/build_manager/db/queries/v2.clj or appropriate, and add tests in test/org/restivo/build_manager/db/queries/v2_test.clj or appropriate.
- The munging functions in src/org/restivo/build_manager/api/response/v1.clj or src/org/restivo/build_manager/api/response/v2.clj might be sufficient for your needs. If not, add what you need. 
- Finally, add the API endpoints in builds-manager/src/org/restivo/build_manager/api/v2.clj or similar, and tests in test/org/restivo/build_manager/api/v2.clj or similar.


## Adding point-in-time query capability
This is one of the most powerful features of Datomic: you can query the database as-of a certain date.
- Under the Yada specification for the api, add under :parameters:
```clojure
:query {(s/optional-key :date) m/LongDate}
```
- Change the reponse to look like:
```clojure
 (let [{:keys [path query body]} (:parameters ctx)
        {:keys [whatever-you-need]} path-or-body
        {:keys [date]} query
        db (if date
             (db/as-of  db-state date)
             (db/latest db-state))]
			 ```
- Update your test cases to add "?date=xxxxxx" to exercise this code path
That's all it takes! the db value is now pinned to that date, and any further operations you do on it (searching/querying) will be per that date.
