(ns user)



;;;  for queries tests
(comment

  ;; wip.
  ;; TODO: put in more artifacts, and more transitions, and more tags
  ;; TODO: do the OR vs AND
  
  (ulog/spewer
   (let [db (db/latest @test-db)
         tags [{:key "release" :value "test-release"}
               {:key "not-here" :value "missing"}]
         ts (ts/timestamp->inst 20170331162923)
         tag-ids (find-tags db tags)]
     (d/q '[:find (pull ?t [*])
            :in $ [?tid] ?ts 
            :where
            [?t :transition/timestamp ?ts]
            [?t :transition/tags ?tid]
            ]
          db
          tag-ids
          ts)))



  

  )

(comment




  ;; all tags
  (let [db (db/latest db/db)]
    (->> (d/q '[:find (pull ?tid [:tag/key :tag/value])
                :in $ 
                :where
                [?t :transition/tags ?tid]
                ]
              db)
         ulog/spewer))




  (log/error *e)

  ;; ffrom prod db
  (ulog/spewer
   (let [db (db/latest db/db)
         tags [{:key "branch", :value "develop"}
               {:key "stage", :value "smoke-pass"}
               {:key "reporter", :value "jenkins-ci"}]]
     (tags->most-recent-transition db tags)))

  
  )


(comment

  (ulog/spewer
   (branch-type->latest-artifact-timestamps (db/latest db/db)
                                            "develop"
                                            :artifact.type/docker))

  
  (ulog/spewer
   (branch-type->latest-artifacts (db/latest db/db)
                                  "develop"
                                  :artifact.type/docker))


  (ulog/spewer
   (let [db (db/latest db/db)]
     (d/q '[:find ?h
            :in $ 
            :with ?a
            :where
            [?a :artifact/name ""]
            [?a :artifact/short-hash ?h]
            ]
          db)))


  ;; TODO: REMOOOOVE YAAAA!
  (defn all-by-kv
    "Returns all entity-ids for a particular key and value"
    [db k v]
    (->>      (d/q '[:find ?a
                     :in $  ?id ?n
                     :where
                     [?a ?id ?n]
                     ]
                   db
                   k
                   v)
              (map first)))
  
  (ulog/spewer
   (let [db (db/latest db/db)]
     (all-by-kv db :artifact/name "")))

  (ulog/spewer  
   (d/transact (db/db :conn) (let [db (db/latest db/db)]
                               (for [i (all-by-kv db :artifact/name "")]
                                 {:db/excise i}))))


  

  (ulog/spewer
   (all-artifact-names db/db))
  
  
  


  

  )


;;; for model tests
(comment

  (dbt/setup-fake-db test-db)


  ;; TODO: unit tests for this

  (insert-id {:foo :bar} "test")
  
  (s/validate Artifact {:short-hash "023af9"
                        :name "durp"
                        :type :docker
                        :git-commit "79ebddf"
                        :git-branch "blah"
                        :semantic-version "0.1.12-bogus",
                        :build-number 43
                        :build-timestamp 20170322090528
                        })


  
  (->>  {:short-hash "023af9"
         :name "bar"
         :type :docker
         :git-commit "baz"
         :git-branch "blah"
         :semantic-version "0.1.12-bogus",
         :build-number 43
         :build-timestamp 20170322090528
         }
        json/encode
        (spit "resources/test-data/artifact.json"))

  (let [c (sc/coercer Artifact sc/json-coercion-matcher)]
    (-> "resources/test-data/artifact.json"
        slurp
        (json/decode true)
        c))


  (u/catcher
   (umisc/munge-columns (:artifact value-xforms)
                        {:short-hash "023af9"
                         :name "bar"
                         :type :docker
                         :git-commit "baz"
                         :git-branch "blah"
                         :semantic-version "0.1.12-bogus",
                         :build-number 43
                         :build-timestamp 20170322090528}))
  

  (ulog/spewer
   (->datomic (-> @test-db db/latest) "artifact"
              {:short-hash "023af9"
               :name "bar"
               :type :docker
               :git-commit "bAAz"
               :git-branch "blah"
               :semantic-version "0.1.12-bogus",
               :build-number 43
               :build-timestamp 20170322090528}))


  (->> "resources/test-data/artifact.json"
       slurp
       u/json-key-decode
       (->datomic (db/latest @test-db) "artifact")
       ulog/spewer)

  

  (re-matches #"[0-9a-f]+" "79ebddf")

  
  (u/spewer
   (->datomic "test"
              {:artifacts ["85678ae90ed2" "a3141303458f" "724d66ea6bca"],
               :result :pass,
               :jenkins-job-link "string",
               :test-rail-link "string",
               :timestamp 20170331162923}))

  (u/spewer
   (umisc/munge-columns (xform-value (db/latest db/db) :test)
                        {:artifacts ["85678ae90ed2" "a3141303458f" "724d66ea6bca"],
                         :result :pass,
                         :jenkins-job-link "string",
                         :test-rail-link "string",
                         :timestamp 20170331162923}))

  (map (partial u/ns-keys "tag")
       [{:key "release", :value "test-release"}])
  
  (u/spewer
   (umisc/munge-columns (value-xforms (db/latest db/db) :transition)
                        (-> "resources/test-data/transition.json" slurp (json/decode true))))


  
  
  )
