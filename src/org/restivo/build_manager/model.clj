(ns org.restivo.build-manager.model
  (:require 
   [utilza.repl :as urepl]
   [schema.core :as s]
   ;;   [clj-time.core :as time]
   [mount.core :as mount]
   [clojure.string :as str]
   [cheshire.core :as json]
   [utilza.misc :as umisc]
   [utilza.datomic :as dutil]
   [clj-time.format :as fmt]
   [utilza.core :as utilza]
   [utilza.datomic :as du]
   [clj-time.coerce :as tc]
   [org.restivo.build-manager.timestamp :as ts]
   [org.restivo.build-manager.util :as u]
   [org.restivo.build-manager.db.queries :as q]
   [schema.coerce :as sc]
   [datomic.api :as d]
   [utilza.log :as ulog]
   [taoensso.timbre :as log])
  (:import java.util.Date
           java.util.UUID))


(s/defschema NotEmptyStr
  (s/both s/Str (s/pred (comp not empty?)
                        "Must not be empty")))

(s/defschema URLSafe
  (s/both NotEmptyStr
          (s/pred #(->> % (re-matches #"[0-9a-z-._~]+") boolean)
                  "Must be url-safe lowercase chars and nums")))


(s/defschema HexHash
  "A hash string of hex numbers. No restrictions on length."
  (s/both NotEmptyStr
          (s/pred #(->> % (re-matches #"[0-9a-f]+") boolean)
                  "Must be hex numbers 0-f")))


(s/defschema ArtifactType
  "An ENUM of artifact types"
  (s/pred #{:docker :jar :wheel :tarball :exec :archive :helm-chart}
          "Can only be docker, jar, wheel, tarball, archive, helm-chart, or exec"))



;; Name components may contain lowercase letters, digits and separators.
;; A separator is defined as a period, one or two underscores, or one or more dashes.
;; A name component may not start or end with a separator.
(s/defschema GitBranch
  "A branch name, formatted to meet the docker specs at https://github.com/docker/docker/blob/master/docs/reference/commandline/tag.md"
  NotEmptyStr) ;; TODO: no, require it to fit branch name schema instead! no /, etc

;; TODO: use coercions
(s/defschema UUIDString
  "An UUID as a string"
  (s/both NotEmptyStr (s/pred #(try (java.util.UUID/fromString  %) (catch Exception e))
                              "Must be a valid UUID.")))

(s/defschema LongDate
  "An Date as an integer"
  (s/both s/Int
          (s/pred pos?
                  "Must be a positive number")
          (s/pred #(< 1230768000000 %)
                  "Company didn't even exist before 2009. Your datestamp in millseconds since epoch is too small.")
          (s/pred #(try (tc/from-long %) (catch Exception e))
                  "Must be an epoch time in milliseconds")))

(s/defschema ShortHash
  "A GIT short commit hash"
  (s/both HexHash
          (s/pred #(= 7 (.length %))
                  "Must be short git commit hash of 7 chars")))

(s/defschema Artifact
  "A compiled, publishable artifact of the build pipeline"
  {:short-hash HexHash
   :name NotEmptyStr
   :type (s/both s/Keyword  ArtifactType)
   :git-commit ShortHash
   :git-branch GitBranch
   (s/optional-key :semantic-version) s/Str
   :build-number Long
   :build-timestamp ts/Timestamp})

(s/defschema Tag
  "A K/V open format tag for transitions"
  {:key URLSafe
   :value NotEmptyStr})


(s/defschema EnvironmentPost
  {:name NotEmptyStr ;; natural key
   :artifacts (s/both [HexHash] (s/pred (comp pos? count)) )
   :tags [Tag] ;; TODO: handles empty too?
   })

(s/defschema TransitionPost
  {:artifacts (s/both [HexHash] (s/pred (comp pos? count)) )
   :tags [Tag] ;; TODO: handles empty too?
   :timestamp ts/Timestamp})

(s/defschema Transition
  (assoc TransitionPost
         (s/optional-key :id) s/Uuid))



(s/defschema LatestArtifactByTagsNameBranchRequest
  {:artifact-name NotEmptyStr
   :artifact-type (s/both s/Keyword  ArtifactType)
   :git-branch GitBranch
   :tags [Tag]})

;; these ns's must have ids
(def needs-ids #{:transition :test})

(defn insert-id
  "Takes a map and a key ns. 
   Inserts a keyspaced id into the map, if necessary (needs-id?).
   Namespaces them since we need to know the ns anyway, we're already here"
  [m key-ns]
  (let [k (utilza/ns-key (name key-ns) :id)
        nsk (keyword key-ns)
        needs-id? (nsk needs-ids)]
    (if needs-id?
      (assoc m k (d/squuid))
      m)))

(defn add-db-id
  [m]
  (assoc m :db/id "dbid"))

;; TODO: there is a lot going on here and perhaps some of these anon functions could be broken out for readability/unittests
(defn value-xforms
  "Takes a db and a key for the type of record.
   Returns a map with keys of the datom and a fn to transform for values
   for transacting into datomic."
  [db k]
  (k {:artifact {:type  (partial utilza/ns-key "artifact.type")
                 ;; TODO: trim?
                 :git-commit str/lower-case
                 :build-timestamp ts/timestamp->inst}
      :transition {:artifacts (partial q/artifact-hashes->ids db)
                   :tags (partial map (fn [{:keys [key value] :as kv}]
                                        (-> (u/ns-keys "tag" kv)
                                            ;; XXX note, there is a race condition here by doing the find-tag not in the tx
                                            ;; i.e. someone else may have put a tag in there that's a duplicate,
                                            ;; in between find-tag and when this tx executes.
                                            ;; TODO; maybe do as a tx function, it'll be atomic then
                                            (assoc :db/id  (or (q/find-tag db key value) (d/tempid :db.part/user))))))
                   :timestamp ts/timestamp->inst}
      :environment {:artifacts (partial q/artifact-hashes->ids db)
                    :tags (partial map (fn [{:keys [key value] :as kv}]
                                         (-> (u/ns-keys "tag" kv)
                                             ;; XXX note, there is a race condition here by doing the find-tag not in the tx
                                             ;; i.e. someone else may have put a tag in there that's a duplicate,
                                             ;; in between find-tag and when this tx executes.
                                             ;; TODO; maybe do as a tx function, it'll be atomic then
                                             (assoc :db/id  (or (q/find-tag db key value) (d/tempid :db.part/user))))))
                    }
      }))

;; TODO: spec this, type key-ns
(defn ->datomic
  "Takes a datomic db, a ns (as a string) for the ns of keys, and a map that comes from json.
   Returns a map with all keys ns'ed, for use with datomic"
  [db key-ns m]
  (log/trace "munging into datomic" m key-ns)
  (->> m
       (umisc/munge-columns (value-xforms db (keyword key-ns)))
       (u/ns-keys key-ns)))

;; TODO spec me!
(defn crud-map->tx
  "Takes a datomic db, a string ns, and a map.
   Returns a datomic tx for insertion"
  [db ns m]
  (-> db
      (->datomic ns m)
      (insert-id (keyword ns))
      add-db-id))


(defn get-what-was-inserted
  "Given a crud-like map where 'dbid' was mapped to the :db/id,
   return an entity with all assertions, as a map"
  [{:keys [tempids db-after]}]
  (let [id (get tempids "dbid")]
    (->> id
         (d/entity db-after )
         (dutil/recurse-1-entity db-after))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment


  (re-matches #"^[0-9a-f][0-9a-f\-._]*[0-9a-f]$" "foo")

  

  (fmt/show-formatters)

  
  (tc/to-long (tc/from-string   "2009-01-01"))
  
  
  )
