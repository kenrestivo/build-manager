(ns org.restivo.build-manager.webutils
  (:require 
   [clojure.string :as str]
   [clojure.java.io :as jio]
   [ring.mock.request :as req]
   [mount.core :as mount]
   [clojure.test :as t]
   [org.restivo.build-manager.timestamp :as ts]
   [bidi.bidi :as b]
   [org.restivo.build-manager.model :as m]
   [org.restivo.build-manager.db.queries :as q]
   [utilza.repl :as urepl]
   [schema.core :as s]
   [datomic.api :as d]
   [utilza.java :as ujava]
   [utilza.misc :as umisc]
   [yada.schema :as ys]
   [bidi.schema :as bschem]
   [cheshire.core :as json]
   [schema.utils :as sutil]
   [org.restivo.build-manager.util :as u]
   [utilza.log :as ulog]
   [bidi.ring :as br]
   [byte-streams :as bs]
   [yada.yada :as y]
   [org.restivo.build-manager.log :as l]
   [ring.util.response :as res]
   [taoensso.timbre :as log]
   ))


;;; XXX this is horrible, and doesn't even work properly. this function needs a massive re-think
(def standard-responses {#{400 401 402 403 405 406 409 410 412 414 415 416 417} ;; some rationally-expected 400 errors
                         {:description "Bad request"
                          ;; TODO: csv too? html? text? why not handle them all in produces?
                          :produces "application/json"
                          :response (fn [{:keys [response request error] :as ctx}]
                                      ;; XXX as useful as it may be, crashes :(
                                      #_(when (and request error)
                                          (log/warn  request error))
                                      (when (= clojure.lang.ExceptionInfo (type error))
                                        (log/error (or (some-> error ex-data :error) error) )
                                        ;; TODO: a case dispatch, handle non-exception-info errors with .getmessage maybe
                                        (doseq [e (some-> error ex-data :errors)]
                                          ;; once again, crashes , so can't use.
                                          #_(log/warn "e1:" e request)
                                          ;; by definition :errors is a seq of some kind
                                          (doseq [e1 e]
                                            ;; XXX schema errors need to be taken out and shot. WTF?
                                            (when (= schema.utils.ErrorContainer (type e1))
                                              (let [e2 (some-> e1 :error)]
                                                (when (= schema.utils.NamedError (type e2))
                                                  (log/warn (sutil/named-error-explain e2)))
                                                (when (= schema.utils.ValidationError (type e2))
                                                  (log/warn (sutil/validation-error-explain e2))))))))
                                      (-> response
                                          (merge  {:body {:error (or (some-> error .getMessage) "Bad Request") 
                                                          :debug (or (when (= clojure.lang.ExceptionInfo (type error))
                                                                       (or (some-> error ex-data :errors)
                                                                           (some-> error ex-data :error))) 
                                                                     (some-> error .getMessage)
                                                                     (some-> error ex-data :error .getMessage)
                                                                     )}})
                                          ;; remove it so that it does not throw again
                                          (dissoc :error)))}
                         500 {:description "Error"
                              :produces "application/json"
                              :response (fn [{:keys [response error] :as ctx}]
                                          (log/error error)
                                          (-> response
                                              (merge  {:status 500
                                                       :body {:error "Error. See logs."} })
                                              ;; remove it so that it does not throw again
                                              (dissoc :error)))}})



(def response-404 {:description "not found"
                   ;; TODO: csv too?
                   :produces "application/json"
                   :response (fn [{:keys [response error]}]
                               (-> response
                                   (merge  {:status 404
                                            :body {:error "404"} })
                                   (dissoc :error)))})


