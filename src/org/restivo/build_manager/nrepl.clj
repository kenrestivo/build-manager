(ns org.restivo.build-manager.nrepl
  (:require
   [clojure.tools.nrepl.server :as nrepl]
   [org.restivo.build-manager.log :as dlog] ;; have to have this here so the startup shows up in logs
   [mount.core :as mount]
   [taoensso.timbre :as log]))


(defn start-nrepl
  [settings]
  (when settings
    (log/info "starting nrepl connection" settings)
    (apply nrepl/start-server (apply concat settings))))

(mount/defstate ^{:on-reload :noop}
  nrepl
  :start (some->  (mount/args) :nrepl start-nrepl)
  :stop (some-> nrepl nrepl/stop-server))


