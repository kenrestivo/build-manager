(ns org.restivo.build-manager.conf
  (:require [clj-yaml.core :as yaml]
            [clojure.spec :as s]
            [clojure.test :as t]
            [utilza.file :as f]
            [utilza.log :as ulog]
            [clojure.test :as t]
            [org.restivo.build-manager.spec :as sp] ;; do not ever remove this
            [utilza.coercions :as c]
            [org.restivo.build-manager.util :as u]
            [clojure.java.io :as jio]
            
            ))

(def defaults
  {:db {:uri "datomic:free://localhost:4334/build-manager"}
   :server {:port 8089
            :join? false}
   :log {:level :info}
   })



(defn validate
  "Takes settings map in, and conforms them to type, erroring if they can't be coerced"
  [settings]
  (binding [c/*conform-mode* :utilza.coercions/string]
    (if (s/valid? :org.restivo.build-manager/conf settings)
      (s/conform :org.restivo.build-manager/conf settings)
      (do
        ;; XXX hack. For some reason, explain doesn't print to stdout in repl, so use println.
        (println (s/explain-str :org.restivo.build-manager/conf settings))
        (throw (ex-info "Invalid settings" (s/explain-data :org.restivo.build-manager/conf settings)))))))


(defn read-and-validate
  "Given a conf file path, reads it and parses it. Throws if no good"
  [conf-file]
  ;; TODO: check for file present?
  (->> conf-file
       slurp
       yaml/parse-string
       (merge-with merge defaults)
       validate
       ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UNIT TESTS


;; TODO: make sure the default conf is valid!
(t/deftest valid-default-confs
  (let [dir "defaults/"]
    (doseq [f (->  dir jio/resource jio/as-file str  (f/file-names #".*\.yml$"))]
      (t/testing f)
      (t/is (map? (read-and-validate (str "resources/" dir f)))))))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  (ulog/catcher
   (read-and-validate "resources/defaults/standard-debug.yml"))

  
  (ulog/catcher
   (t/run-tests))
  
  )
