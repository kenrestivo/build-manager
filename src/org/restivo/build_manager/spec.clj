(ns org.restivo.build-manager.spec
  "Specs for data types, used for type checking and generative tests."
  (:require [clojure.spec :as s]
            [utilza.coercions :as c]
            [org.restivo.build-manager.timestamp :as ts]
            [clojure.test.check.generators :as gens]
            [utilza.java :as ujava]
            [clojure.string :as str]
            [clojure.test :as t]
            ))



(s/def :numeric/pos-int (s/and (c/dynamic-conformer integer? :utilza.coercions/int) pos?))
(s/def :numeric/pos-long (s/and (c/dynamic-conformer integer? :utilza.coercions/long) pos?))



(s/def :org.restivo.build-manager.log/level (s/and (c/dynamic-conformer keyword? :utilza.coercions/keyword)
                                                #{:info
                                                  :warn
                                                  :debug
                                                  :trace
                                                  :error}))
(s/def :org.restivo.build-manager.log/spit-filename (s/and string? (comp not empty?)))
(s/def :org.restivo.build-manager.log/logstash-host (s/and string? (comp not empty?)))
(s/def :org.restivo.build-manager.log/logstash-port :numeric/pos-int)

(s/def :org.restivo.build-manager.server/server string?)
(s/def :org.restivo.build-manager.server/join? boolean?)
(s/def :org.restivo.build-manager.server/port :numeric/pos-int)

(s/def :org.restivo.build-manager.db/uri string?)
(s/def :org.restivo.build-manager.db/create? boolean?)

(s/def :org.restivo.build-manager/db (s/keys :req-un [:org.restivo.build-manager.db/uri 
                                                   ]
                                          :opt-un [:org.restivo.build-manager.db/create?]))

(s/def :org.restivo.build-manager/server (s/keys :req-un [
                                                       :org.restivo.build-manager.server/join?
                                                       :org.restivo.build-manager.server/port]))


(s/def :org.restivo.build-manager/nrepl (s/keys :req-un [:org.restivo.build-manager.nrepl/port
                                                      ]
                                             :opt-un [:org.restivo.build-manager.nrepl/bind]))

(s/def :org.restivo.build-manager/log (s/keys :req-un [:org.restivo.build-manager.log/level]
                                           :opt-un [:org.restivo.build-manager.log/spit-filename
                                                    :org.restivo.build-manager.log/logstash-port
                                                    :org.restivo.build-manager.log/logstash-host
                                                    ]))

(s/def :org.restivo.build-manager/conf (s/keys :req-un [:org.restivo.build-manager/log
                                                     :org.restivo.build-manager/server
                                                     :org.restivo.build-manager/db
                                                     ]
                                            :opt-un [:org.restivo.build-manager/nrepl]))




;;; The below is used for generators, which are not yet really in use yet. More of a WIP.


;; Artifact
;; XXX TODO: the short hash must be unique so a pseudo-uuid would be more appropriate
(s/def :org.restivo.build-manager.artifact/short-hash (s/with-gen
                                                     (s/and string? (comp not empty?)
                                                            #(->> %  (re-matches #"[0-9a-f]+") boolean))
                                                     #(gens/fmap (fn [s]  (str/replace s "-" ""))
                                                                 gens/uuid)))

(s/def :org.restivo.build-manager.artifact/git-commit (s/with-gen 
                                                     (s/and
                                                      string?
                                                      (comp not empty?)
                                                      #(= 7 (.length %))
                                                      #(->> %  (re-matches #"[0-9a-f]+") boolean))
                                                     ;; XXX cough, hack. use fmap and gens instead TODO
                                                     #(s/gen #{"38a9dea","99e5e61","d2363d0","2e33ede",
                                                               "e9d1bd2","c5af71e","6632b92","4789388","e11d08e","7372955",
                                                               "b5686af","c006b21","30fefde","802d620"})))
(s/def :org.restivo.build-manager.artifact/build-number (s/and integer? pos?))
(s/def :org.restivo.build-manager.artifact/git-branch (s/and string? (comp not empty?)))
(s/def :org.restivo.build-manager.artifact/semantic-version string?)
(s/def :org.restivo.build-manager.artifact/build-timestamp (s/with-gen
                                                          (s/and
                                                           integer?
                                                           pos?
                                                           ts/valid-timestamp?)
                                                          #(gens/fmap (fn [x]
                                                                        (-> x java.util.Date. ts/->timestamp))
                                                                      (gens/choose 1490662081604 1521766081604))))
(s/def :org.restivo.build-manager.artifact/name (s/and string? (comp not empty?)))
(def legal-artifact-types #{:docker :jar :wheel :tarball :exec :archive :helm-chart})
(s/def :org.restivo.build-manager.artifact/type (s/with-gen
                                               (s/and  keyword? legal-artifact-types)
                                               #(s/gen legal-artifact-types)))

(s/def :org.restivo.build-manager/artifact (s/keys :req-un [:org.restivo.build-manager.artifact/short-hash
                                                         :org.restivo.build-manager.artifact/type
                                                         :org.restivo.build-manager.artifact/name
                                                         :org.restivo.build-manager.artifact/git-commit
                                                         :org.restivo.build-manager.artifact/build-number
                                                         :org.restivo.build-manager.artifact/build-timestamp
                                                         :org.restivo.build-manager.artifact/git-branch
                                                         ]
                                                :opt-un [:org.restivo.build-manager.artifact/semantic-version]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; UNIT TESTS

(t/deftest conf-without-coercion
  ;; TODO!!
  )

(t/deftest conf-with-coercion
  (binding [c/*conform-mode* :utilza.coercions/string]
    ;; TODO!!
    ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  (ujava/pseudo-uuid)

  (gens/generate (gens/list-distinct gens/string-alphanumeric))


  (gens/generate (gens/fmap str gens/uuid))

  (require '[utilza.repl :as urepl])

  (+ 1490662081604 (* 1000 60 60 24 30 12))

  
  )
