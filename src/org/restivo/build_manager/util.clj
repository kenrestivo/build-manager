(ns org.restivo.build-manager.util
  (:require 
   [mount.core :as mount]
   [clj-time.coerce :as tc]
   [clojure.walk :as walk]
   [taoensso.timbre :as log]
   [utilza.java :as ujava]
   [clojure.spec.gen :as gen]
   [utilza.log :as ulog]
   [io.aviso.repl :as pst]
   [clj-time.format :as fmt]
   [utilza.uuid :as uu]
   [utilza.core :as utilza]
   [utilza.misc :as umisc]
   [clojure-csv.core :as csv]
   [cheshire.core :as json]
   [clj-time.coerce :as tc]
   [clojure.test :as t]
   [clojure.java.io :as jio]
   [utilza.coercions :as c]
   [clojure.test :as t]
   [clojure.spec :as s]
   ))


(def commands {})

(defn json-key-decode
  [s]
  (json/decode s true))

(defn short-uuid
  []
  (-> (java.util.UUID/randomUUID)  uu/uuid->short-url))

;; TODO: definitely fdef this with string and map inputs
(defn ns-keys
  "Convert all the keys in a map m to ns n"
  [ns m]
  (utilza/xform-keys (partial utilza/ns-key ns) m))


(defn nested-colls->sets
  "Takes a map, and returns that map with any vectors or seqs within turned into sets.
   Used for equality testing where the vectors/seqs might not be in the same order."
  [m]
  ;; TODO; postwalk maybe?
  (walk/prewalk (fn [x]
                  (if (and (or (seq? x)
                               (vector? x))
                           (not (map-entry? x)))
                    (set x)
                    x))
                m))

;; from  utilza
(defn atom?
  "Simply tests that x is an atom"
  [x]
  (instance? clojure.lang.Atom x))


(defn get-usage-text
  []
  (->> "usage.txt"
       jio/resource
       slurp))


(defn usage
  "returns usage text"
  []
  (format "USAGE:\n   java -jar %s <conf file>\n %s"
          (ujava/get-jar)
          (get-usage-text)))



(defn filter-useless
  "*sigh*. If you supply no args, Java gives you an array with a nil in it."
  [args]
  (if (-> args first nil?)
    (rest args)
    args))



(defn throw-usage!
  []
  (throw (ex-info (usage) {})))



(defn debug-request
  [_ ctx]
  (ulog/catcher
   (log/info (some-> ctx :request keys))
   "request received, and logged x"))

(defn moderate-spew
  [m]
  (binding [*print-length* 10000 *print-level* 10000]
    (->> m
         (#(with-out-str (clojure.pprint/pprint %))))))


(defn munge-all-columns
  "Takes a coll of maps and a key map. Returns them with all the values coerced via key-map"
  [key-map ms]
  (for [m ms]
    (umisc/munge-columns key-map m)))


(defn string-and-nameify
  "CSV wants only strings. Takes a map, and converts keywords 
  into strings in both keys and values."
  [m]
  (into {}
        (for [[k v] m]
          [(name k) ((if (keyword? v) name str) v)])))

(defn ->csv
  "Takes a sequence of maps. Returns a CSV string"
  [ms]
  (->> ms
       (map string-and-nameify)
       umisc/uncolumnify
       csv/write-csv))


(defn handler-method-keys-set
  "Takes a BIDI/YADA routing result, and returns the set of methods supported"
  [m]
  (-> m
      (dissoc :yada.swagger/spec)
      :handler
      :methods
      keys
      set))

(defn now-long
  "Returns the current date/time as a long in milleseconds from epoch"
  []
  (tc/to-long (java.util.Date.)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; unit tests
(t/deftest munge-column-test
  (let [key-map {:foo name}]
    (t/is (= [] (munge-all-columns key-map [])))
    (t/is (= [{:foo "bar"}] (munge-all-columns key-map [{:foo :bar}])))
    (t/is (= [{:baz :bar}] (munge-all-columns key-map [{:baz :bar}])))))

(t/deftest csv-export
  (t/is (= "foo,buh,gah,baz\nbar,hmm,293923,quux\n" (->csv [{:foo "bar",
                                                             :buh :hmm
                                                             :gah 293923
                                                             :baz "quux"}])))
  (t/is (= "\n" (->csv []))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  (ulog/catcher
   (gen/sample (s/gen :org.restivo.build-manager/artifact)))


  (ulog/catcher
   (gen/sample (s/gen  :org.restivo.build-manager.artifact/build-timestamp)))

  (t/run-tests)  


  
  
  )
