(ns org.restivo.build-manager.db
  (:require [utilza.repl :as urepl]
            [org.restivo.build-manager.log :as l] ;; want this running first
            [mount.core :as mount]
            [datomic.samples.schema :as s]
            [taoensso.timbre :as log]
            [utilza.log :as ulog]
            [org.restivo.build-manager.util :as u]
            [clojure.string :as str]
            [clojure.java.io :as jio]
            [utilza.datomic :as dutils]
            [clojure.edn :as edn]
            [datomic.api :as d])
  (:import java.util.UUID))

(defn latest
  "Utility to get the latest db value"
  [db]
  (some-> db :conn d/db))


(defn as-of
  "Utility to get the db as of a certain date"
  [ db date]
  (log/trace "as of" date db)
  (some-> db
          :conn
          d/db
          (d/as-of date)))

(defn create-db
  "Xewrw  new db"
  [{:keys [uri]}]
  (d/create-database uri))


(defn read-schema-file
  [fname]
  (-> fname
      jio/resource
      dutils/read-datomic-edn))


(defn load-schema!
  "Loads the schema from a file in resources"
  ([conn fname]
   (let [schema-map (read-schema-file fname)
         schemas (keys schema-map)]
     (apply s/ensure-schemas conn :org.restivo/build-manager schema-map schemas)))
  ([conn]
   (load-schema! conn "schemas/schema.edn")))


(defn start
  "Takes db settings, returns a map of the uri and the connection.
   If create? is set, creates a new db and loads its schema if it is a new db."
  [{:keys [uri create?]}]
  ;; TODO: just use get-database-names and always check to make sure it exists first
  (when create?
    (log/info "You have create? set in your config, so creating the database.")
    (d/create-database uri))
  (let [conn  (d/connect uri)]
    (log/info "Making sure the schema is up to date")
    (load-schema! conn)
    {:uri uri
     :conn conn}))


#_(defn restart-from-scratch!
    "DANGEROUS do not use or only with extreme caution.
   Takes db mount state.
   Deletes database and recreates it blank with new schema"
    [db]
    (log/warn "deleting database and recreating it empty!")
    #_(some-> db :conn d/release)
    #_(some-> db :uri d/delete-database)
    (-> db create-db)
    (mount/start db)
    (some-> db :conn load-schema!))



(mount/defstate ^{:on-reload :noop}
  db
  :start (-> (mount/args) :db start)
  :stop  (d/shutdown false))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  (-> (mount/args) :db  create-db)
  
  (restart-from-scratch! db)

  
  ;; (dutils/load-from-file "resources/schemas/schema.edn" (:conn db))

  ;; TODO; need a proper datomic migrator
  (ulog/spewer
   @(d/transact (:conn db)
                [
                 
                 ]))

  (str (d/squuid))

  (UUID/fromString (str (d/squuid)))
  
  (ulog/spewer
   (-> "schemas/schema.edn"
       read-schema-file))

  
  
  )

