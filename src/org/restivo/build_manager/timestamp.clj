(ns org.restivo.build-manager.timestamp
  (:require 
   [utilza.repl :as urepl]
   [schema.core :as s]
   [clojure.string :as str]
   [cheshire.core :as json]
   [utilza.misc :as umisc]
   [org.restivo.build-manager.util :as u]
   [clj-time.format :as fmt]
   [clojure.test :as t]
   [utilza.core :as utilza]
   [clj-time.coerce :as tc]
   [schema.coerce :as sc]
   [taoensso.timbre :as log])
  (:import java.util.Date))




(def min-timestamp 20170320000000)



(def timestamp-formatter (fmt/formatter "yyyyMMddHHmmss"))


(defmulti ->timestamp
  "Converts various things to a BUILD_TIMESTAMP style long timestamp"
  type)


(defmethod ->timestamp org.joda.time.DateTime
  [d]
  (Long/parseLong (fmt/unparse timestamp-formatter  d)))

(defmethod ->timestamp java.util.Date
  [d]
  (->timestamp (tc/from-date d)))


(defmethod ->timestamp java.lang.String
  [d]
  (Long/parseLong d))


(defmethod ->timestamp java.lang.Long
  [n]
  n)


(defmethod ->timestamp :default
  [x]
  (log/error "unimplemented timestamp conversion for" (type x) x))

(defmulti timestamp->inst
  "Converts a BUILD_TIMESTAMP into an j.u.d inst"
  type)

(defmethod timestamp->inst java.lang.Long
  [x]
  (some-> x str timestamp->inst))

(defmethod timestamp->inst java.lang.String
  [x]
  (tc/to-date (fmt/parse timestamp-formatter x)))



(defmulti datomic->date-time
  type)

(defmethod datomic->date-time java.lang.String
  [d]
  (fmt/parse (:date-time fmt/formatters) d))

(defmethod datomic->date-time java.util.Date
  [d]
  d)

(defmethod datomic->date-time  org.joda.time.DateTime
  [d]
  d)

(defmethod datomic->date-time  :default
  [d]
  (throw (ex-info "why is datomic returning a date as a" {:type (type d)
                                                          :date d})))


(defn valid-timestamp?
  [ts]
  (try
    (-> ts
        timestamp->inst boolean)
    (catch Exception e
      (log/warn "bad timestamp" e))))


(s/defschema Timestamp
  "The Jenkins weird $BUILD_TIMESTAMP which is a long but represents a yyMMddhhmmss value"
  (s/both Long
          (s/pred pos?
                  "Must be a positive number")
          (s/pred valid-timestamp?
                  "Must be a legal date, in the format yyyyMMddhhmmss")
          (s/pred #(< min-timestamp %)
                  "Must be after March 2017")))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; TESTS

(t/deftest from-timestamp
  (t/is (=   (timestamp->inst 20170330193842) #inst "2017-03-30T19:38:42.000-00:00"))
  (t/is (=   (timestamp->inst 20170322090528) #inst "2017-03-22T09:05:28.000-00:00"))
  (t/is (=   (timestamp->inst "20170322090528") #inst "2017-03-22T09:05:28.000-00:00")))


(t/deftest to-timestamp
  (t/is (= (->timestamp #inst "2017-03-22T09:05:28.000-00:00") 20170322090528))
  (t/is (= 20170331162935 (-> "2017-03-31T16:29:35.000Z" datomic->date-time ->timestamp)))
  (t/is (thrown? Exception  (timestamp->inst "20179922090528"))))

(t/deftest round-trip
  (t/testing "round trip between timestamp and date")
  (let [ts "20170322090528"
        d #inst "2017-03-22T09:05:28.000-00:00"]
    (t/is (= ts (-> ts  timestamp->inst ->timestamp str)))
    ;; TODO: this might do with some generative spec testing on random dates
    (t/is (= d (-> d ->timestamp timestamp->inst)))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  (type (fmt/parse timestamp-formatter "20170322090528"))

  (t/run-tests)


  (timestamp->inst 20170508200929)

  
  )
