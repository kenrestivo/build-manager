(ns org.restivo.build-manager.db.queries
  (:require [utilza.repl :as urepl]
            [taoensso.timbre :as log]
            [org.restivo.build-manager.db :as db]
            [utilza.log :as ulog]
            [org.restivo.build-manager.util :as u]
            [clojure.string :as str]
            [utilza.datomic :as dutils]
            [clojure.edn :as edn]
            [datomic.api :as d]))

;; the data format to return
(def nested-artifact
  [:artifact/short-hash
   :artifact/name
   :artifact/git-commit
   :artifact/git-branch
   :artifact/semantic-version
   :artifact/build-timestamp
   :artifact/build-number
   {:artifact/type [:db/ident]}])

;; the data format to return
(def nested-transitions
  [:transition/id
   :transition/timestamp
   {:transition/tags [:tag/key
                      :tag/value]}
   {:transition/artifacts nested-artifact}])

(defn all-artifact-names
  "Returns all names of artifacts"
  [db]
  (ffirst (d/q '[:find (distinct ?name)
                 :in $
                 :with ?a
                 :where
                 [?a :artifact/name ?name]]
               db)))



(defn branch-type->latest-artifact-timestamps
  "Takes a seq of tag ids and a db.
   Returns the maximum timestamp of all transitions with those tag ids"
  [db branch-name artifact-type]
  (d/q '[:find ?n (max ?ts)
         :in $ ?b ?t
         :with ?a
         :where
         [?a :artifact/git-branch ?b]
         [?a :artifact/type ?t]
         [?a :artifact/name ?n]
         [?a :artifact/build-timestamp ?ts]
         ]
       db
       branch-name
       artifact-type
       db))


(defn name-branch->latest-artifact-timestamps
  "Takes a seq of tag ids and a db.
   Returns the maximum timestamp of all transitions with those tag ids"
  [db artifact-name branch-name]
  (d/q '[:find ?n (max ?ts)
         :in $ ?n ?b
         :with ?a
         :where
         [?a :artifact/name ?n]
         [?a :artifact/git-branch ?b]
         [?a :artifact/build-timestamp ?ts]
         ]
       db
       artifact-name
       branch-name
       db))


(defn name-branch->latest-artifacts
  "Given a db and a artifact-name and branch name , returns a coll of the maps with the latest artifacts for each name"
  [db artifact-name branch-name ]
  (->> (d/q {:find [(list 'pull '?a  nested-artifact)]
             :in '[$ ?n ?b  [[?n ?ts]]]
             :where '[[?a :artifact/build-timestamp ?ts]
                      [?a :artifact/name ?n]
                      [?a :artifact/git-branch ?b]
                      ]}
            db
            artifact-name
            branch-name
            (name-branch->latest-artifact-timestamps db artifact-name branch-name))
       (map first)))

(defn branch-type->latest-artifacts
  "Given a db and a branch name, returns a coll of the maps with the latest artifacts for each name"
  [db branch-name artifact-type]
  (->> (d/q {:find [(list 'pull '?a  nested-artifact)]
             :in '[$ ?b ?type [[?n ?ts]]]
             :where '[[?a :artifact/build-timestamp ?ts]
                      [?a :artifact/name ?n]
                      [?a :artifact/type ?t]
                      [?a :artifact/git-branch ?b]
                      ]}
            db
            branch-name
            artifact-type
            (branch-type->latest-artifact-timestamps db branch-name artifact-type))
       (map first)))




(defn artifact-hash->id
  [db h]
  (log/trace "checking hash for" h)
  (or (dutils/one-index db :artifact/short-hash h)
      (let [msg (format "There is no artifact in the db with short-hash of %s" (str h))]
        (throw (ex-info msg {:status 404
                             :error msg})))))


(defn artifact-hashes->ids
  [db hs]
  (log/trace "checking hashes for" hs)
  (for [h hs]
    (artifact-hash->id db h)))




(defn all-or-nothing
  [xs]
  (when (every? identity xs)
    xs))


(defn find-tag
  "Given a db and a key string and value string,
   returns the eid of the tag entity matching those"
  [db k v]
  (ffirst
   (d/q '[:find ?e
          :in $  ?k ?v
          :where
          [?e :tag/value ?v]
          [?e :tag/key ?k]]
        db
        k
        v)))


(defn find-tags
  "Takes key/value tags and a db.
  Returns the tag ids, or nothing if all are not present"
  [db tags]
  (all-or-nothing
   (for [{:keys [key value]} tags]
     (find-tag db key value))))


(defn tag-ids->expressions
  "Takes seq of integer tag entity ids.
  Returns vectors of expressions for datomic AND"
  [tag-ids]
  {:pre [(every? int? tag-ids)]}
  (for [t tag-ids]
    ['?t :transition/tags t]))


;; better tag combo searches

(defn tag-ids->max-transition-timestamp
  "Takes a seq of tag ids and a db.
   Returns the maximum timestamp of all transitions with those tag ids"
  [db tag-ids]
  (ffirst (d/q {:find '[(max ?timestamp)]
                :in '[$]
                :where  (conj
                         (tag-ids->expressions tag-ids)
                         '[?t :transition/timestamp ?timestamp])}
               db)))

(defn tags->most-recent-transition
  "Takes a db, and a sequence of tag maps [{:key ... :value}].
  Returns the most recent transition which matches all (and)"
  [db tags]
  (when-let [tag-ids (find-tags db tags)]
    (some->> (tag-ids->max-transition-timestamp db tag-ids)
             (d/q {:find [(list 'pull '?t nested-transitions)]
                   :in '[$ ?timestamp]
                   :where (conj (tag-ids->expressions tag-ids)
                                '[?t :transition/timestamp ?timestamp])}
                  db)
             ffirst)))


(defn artifact-name-branch-tag-ids->max-transition-timestamp
  "Takes a seq of tag key/value maps, an artifact name, an artifact type, and branch and a db.
   Returns the maximum timestamp of the artifact with those."
  [db artifact-name artifact-type artifact-branch tags]
  {:pre [(not (nil? db))]}
  (when-let [tag-ids (find-tags db tags)]
    (ffirst (d/q {:find '[(max ?timestamp)]
                  :in '[$ ?n ?y ?b]
                  :where  (concat
                           (tag-ids->expressions tag-ids)
                           '[[?t :transition/artifacts ?a]
                             [?a :artifact/name ?n]
                             [?a :artifact/type ?y]
                             [?a :artifact/git-branch ?b]
                             [?t :transition/timestamp ?timestamp]])}
                 db
                 artifact-name
                 artifact-type
                 artifact-branch))))




(defn artifact-name-branch-tag-ids->latest-artifact
  "Takes a db, and a sequence of tag maps [{:key ... :value}].
  Returns the most recent transition which matches all (and)"
  [db artifact-name artifact-type artifact-branch tags]
  (->> (d/q {:find [(list 'pull '?a  nested-artifact)] 
             :in '[$ ?n ?y ?b  ?ts]
             :where '[[?a :artifact/name ?n]
                      [?a :artifact/git-branch ?b]
                      [?t :transition/artifacts ?a]
                      [?t :transition/timestamp ?ts]]}
            db
            artifact-name
            artifact-type
            artifact-branch
            (artifact-name-branch-tag-ids->max-transition-timestamp db artifact-name artifact-type artifact-branch tags))
       ffirst))




(defn artifacts-by-name-in-env
  "Takes a db, an environment name, and an artifact name
  Returns the short hashes of artifacts matching that name in that env."
  [db environment-name artifact-name]
  (->> (d/q {:find '[?h] 
             :in '[$ ?en ?an]
             :where '[[?e :environment/name ?en]
                      [?e :environment/artifacts ?a]
                      [?a :artifact/name ?an]
                      [?a :artifact/short-hash ?h]]}
            db
            environment-name
            artifact-name)
       (map first)))


(defn remove-and-replace-env-tx
  "Generates query for removing and replacing named artifacts from an env. 
   Takes environment name, artifact short hashes to remove, and artifact to add. Returns the tx"
  [env-name artifact-name remove-hashes add-hash]
  {:pre [(coll? remove-hashes)]}
  (-> (for [short-hash remove-hashes]
        [:db/retract [:environment/name env-name ] :environment/artifacts [:artifact/short-hash short-hash]])
      (conj [:db/add [:environment/name env-name ] :environment/artifacts [:artifact/short-hash add-hash]])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment


  (ulog/spewer
   (all-artifact-names (db/latest db/db)))


  (ulog/spewer
   )


  (artifact-hashes->ids (db/latest db/db)  ["85678ae90ed2" "a3141303458f" "724d66ea6bca"])

  (artifact-hash->id (db/latest db/db)  "85678ae90ed2")




  )
