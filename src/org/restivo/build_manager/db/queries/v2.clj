(ns org.restivo.build-manager.db.queries.v2
  (:require [utilza.repl :as urepl]
            [taoensso.timbre :as log]
            [org.restivo.build-manager.db :as db]
            [org.restivo.build-manager.db.queries :as q]
            [utilza.log :as ulog]
            [org.restivo.build-manager.util :as u]
            [clojure.string :as str]
            [utilza.datomic :as dutils]
            [clojure.edn :as edn]
            [datomic.api :as d]))




(defn branch->artifacts
  "Given a db and a branch name, 
  returns a coll of the maps with all artifacts, sorted by date, in reverse order"
  [db branch-name]
  (->> (d/q {:find [(list 'pull '?a  q/nested-artifact)]
             :in '[$ ?b]
             :where '[[?a :artifact/git-branch ?b]
                      ]}
            db
            branch-name)
       (map first)
       (sort-by :artifact/build-timestamp)
       reverse))



(defn branch-type->artifacts
  "Given a db and a branch name, 
  returns a coll of the maps with all artifacts, sorted by date, in reverse order"
  [db branch-name artifact-type]
  (->> (d/q {:find [(list 'pull '?a  q/nested-artifact)]
             :in '[$ ?b ?t]
             :where '[[?a :artifact/git-branch ?b]
                      [?a :artifact/type ?t]
                      ]}
            db
            branch-name
            artifact-type)
       (map first)
       (sort-by :artifact/build-timestamp)
       reverse))

