(ns org.restivo.build-manager.api.common
  (:require 
   [clojure.string :as str]
   [clojure.java.io :as jio]
   [schema.core :as s]
   [ring.mock.request :as req]
   [mount.core :as mount]
   [clojure.test :as t]
   [org.restivo.build-manager.timestamp :as ts]
   [org.restivo.build-manager.webutils :as wutil]
   [yada.context :as ctx]
   [bidi.bidi :as b]
   [org.restivo.build-manager.model :as m]
   [org.restivo.build-manager.db.queries :as q]
   [org.restivo.build-manager.db :as db]
   [hiccup.core :as h]
   [utilza.repl :as urepl]
   [schema.core :as s]
   [datomic.api :as d]
   [utilza.java :as ujava]
   [utilza.misc :as umisc]
   [yada.schema :as ys]
   [bidi.schema :as bschem]
   [utilza.core :as utilza]
   [cheshire.core :as json]
   [org.restivo.build-manager.util :as u]
   [bidi.ring :as br]
   [byte-streams :as bs]
   [yada.yada :as y]
   [org.restivo.build-manager.log :as l]
   [ring.util.response :as res]
   [taoensso.timbre :as log]
   ))


(defonce debug-atom (atom nil))

(defn ping-route
  "A hello world route"
  []
  ["/ping" (y/as-resource "Pong\n")])

(defn version-route
  "Display the version of the db-state-manager jar running"
  []
  ["/version" (y/as-resource (ujava/get-project-properties l/group l/artifact))])


(defn inspect-post
  [db-state]
  ["/inspect-post"
   (y/resource
    {:id :org.restivo.build-manager.debug/inspect-post
     :responses wutil/standard-responses
     :methods {:post {:summary "Look at what was posted"
                      :produces {:media-type "application/json"}
                      :parameters {:body {s/Any s/Any}}
                      :consumes [{:media-type #{"application/json"}
                                  :charset "UTF-8"}]
                      :response (fn show
                                  [ctx]
                                  (some-> ctx :parameters log/info))}}})])


(defn throw-resource
  [db-state]
  ["/throw"
   (y/resource
    ;; this doesn't actually show the throw yo.
    {:id :org.restivo.build-manager.debug/throw
     :responses wutil/standard-responses
     :methods {:get {:summary "Throw it all"
                     :description "Commits suicide"
                     :produces {:media-type "application/json"}
                     :response (fn blow-up
                                 [ctx]
                                 (/ 1 0))}}})])



(defn rest-parameters-test
  "test rest params"
  []
  ["/path-vals" [["" (y/as-resource "nothing")]
                 [["/" :id] (y/resource
                             {:parameters {:path {:id String}}
                              :id :org.restivo.build-manager.debug/path-vals
                              :responses wutil/standard-responses
                              :swagger/tags ["debug"]
                              :methods {:get {:produces [{:media-type "application/json"}
                                                         {:media-type "text/csv"}]
                                              :response (fn [ctx]
                                                          (log/trace ctx)
                                                          (let [id (get-in ctx [:parameters :path :id])]
                                                            (log/trace "path vals test:" id)
                                                            (json/encode {:got id}))) }}})]]])


;; NOTE: do NOT do this if keys/secrets are in either! must filter REDACTED those in that case
(defn status
  "Lifted shameslessly from y/edge repo, with mods for json too"
  []
  ["/status" (y/resource
              {:id :org.restivo.build-manager.debug/status
               :methods
               {:get
                {:produces [{:media-type "application/json"}
                            {:media-type "text/html"}]
                 :response (fn [ctx]
                             (let [env (into {} (System/getenv))
                                   props (into {} (System/getProperties))
                                   t (ctx/content-type ctx)]
                               (case t
                                 "application/json"  (json/encode {:props props
                                                                   :env env})
                                 "text/html" (h/html
                                              [:body
                                               [:div
                                                [:h2 "System properties"]
                                                [:table
                                                 (for [[k v] (sort props)]
                                                   [:tr
                                                    [:td [:pre k]]
                                                    [:td [:pre v]]]
                                                   )]]
                                               [:div
                                                [:h2 "Environment variables"]
                                                [:table
                                                 (for [[k v] (sort env)]
                                                   [:tr
                                                    [:td [:pre k]]
                                                    [:td [:pre v]]]
                                                   )]]])
                                 (log/warn "unknown context type" t))))}}})])


(defn sources
  []
  ["/sources/"
   (y/resource
    {
     ;; We enable path-info which means that the route matches any
     ;; path that matches /sources/*.
     :path-info? true

     :properties (fn [ctx]
                   ;; Use the path-info as the relative path of a source file
                   (if-let [f (y/safe-relative-file
                               (System/getProperty "user.dir")
                               (-> ctx :request :path-info))]
                     ;; Valid file path, possibly we exist too. We add the file as a
                     ;; property of the resource.
                     {:exists? (.exists f)
                      ::file f}
                     ;; Invalid file, so we don't exist
                     {:exists? false}))
     

     :methods {:get {
                     ;; Serve as 'raw' (text/plain)
                     :produces {:media-type "text/plain"
                                :charset "UTF-8"}
                     ;; Return the file we found as the response.
                     :response #(-> % :properties ::file)}}})])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment
  
  ;; huh, works
  (slurp (y/safe-relative-file
          (System/getProperty "user.dir")
          "project.clj")) 

  )
