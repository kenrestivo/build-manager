(ns org.restivo.build-manager.api.v1
  (:require 
   [clojure.string :as str]
   [clojure.java.io :as jio]
   [ring.mock.request :as req]
   [mount.core :as mount]
   [clojure.test :as t]
   [org.restivo.build-manager.timestamp :as ts]
   [org.restivo.build-manager.webutils :as wutil]
   [org.restivo.build-manager.api.common :as apic]
   [org.restivo.build-manager.api.response.v1 :as v1]
   [org.restivo.build-manager.api.response.common :as resp]
   [yada.context :as ctx]
   [bidi.bidi :as b]
   [org.restivo.build-manager.model :as m]
   [org.restivo.build-manager.db.queries :as q]
   [org.restivo.build-manager.db :as db]
   [utilza.repl :as urepl]
   [schema.core :as s]
   [datomic.api :as d]
   [utilza.java :as ujava]
   [utilza.misc :as umisc]
   [yada.schema :as ys]
   [bidi.schema :as bschem]
   [utilza.core :as utilza]
   [cheshire.core :as json]
   [org.restivo.build-manager.util :as u]
   [bidi.ring :as br]
   [byte-streams :as bs]
   [yada.yada :as y]
   [org.restivo.build-manager.log :as l]
   [ring.util.response :as res]
   [taoensso.timbre :as log]
   ))


(defonce debug-atom (atom nil))


;; TODO: change to git-branch to make the coercion/validation work! branch-name is bogus.
(defn latest-artifacts-by-branch-type
  [db-state]
  ["/latest-artifacts-by-branch-type"
   (y/resource
    {:parameters {:query {:branch-name String ;; TODO: no, require it to fit branch name schema instead
                          :type String}} ;; TODO: no, require it to be a valid type
     :responses wutil/standard-responses
     :swagger/tags ["reports"]
     :methods {:get {:summary "Get latest of all artifacts for a particular branch"
                     :description (str "Gets latest of all artifacts for a branch supplied. "
                                       "type: must be one of docker, jar, wheel, tarball, archive, helm-chart, or exec. "
                                       "branch-name: must be a legal docker-formatted git branch with /'s removed")
                     :produces [{:media-type "application/json"}
                                {:media-type "text/csv"}]
                     :response #(v1/ctx->latest-artifacts-by-branch-type db-state %)}}})])


(defn latest-transition-by-tags
  [db-state]
  ["/latest-transition-by-tags"
   (y/resource
    {:responses wutil/standard-responses
     :swagger/tags ["reports"]
     :methods {:post {:summary "Get latest of all transitions with specified tags"
                      :description (str "Gets latest of all transitions for tags specified")
                      :consumes [{:media-type #{"application/json"}
                                  :charset "UTF-8"}]
                      :parameters {:body {:tags [m/Tag]}}
                      :produces [{:media-type "application/json"}
                                 {:media-type "text/csv"}]
                      :response #(v1/ctx->latest-transition-by-tags  db-state %)}}})])

(defn artifact
  "Routing for the artifact record operations"
  [db-state]
  ["/artifact"
   (y/resource
    {:summary "An artifact, such as a Docker image, a JAR file, wheel, archive, helm-chart, tarball, etc"
     :methods {:post {:summary "Insert data for an artifact"
                      :description (str "Create a new artifact record: "
                                        ;; XXX hideous hack
                                        ;; need cleaner way to show schemas in a readable format
                                        m/Artifact)
                      :parameters {:body m/Artifact}
                      :consumes [{:media-type #{"application/json"}
                                  :charset "UTF-8"}]
                      :response (partial v1/insert! db-state "artifact")}
               }
     :responses wutil/standard-responses})])




(defn transition
  "Routing for the transition record operations"
  [db-state]
  ["/transition"
   (y/resource
    {:summary "A transition run."
     :description "Data about a particular transition for a set of artifacts"
     :methods {:post {:summary "Insert data for a transition run. Artifacts are identified by short-hash'es"
                      :description (str "Create a new transition record: "
                                        ;; XXX hideous hack
                                        ;; need cleaner way to show schemas in a readable format
                                        m/TransitionPost)
                      :parameters {:body m/TransitionPost}
                      :consumes [{:media-type #{"application/json"}
                                  :charset "UTF-8"}]
                      :response (partial v1/insert! db-state "transition")}}
     :responses wutil/standard-responses})])


(defn reports
  [db-state]
  ["/reports" [(latest-artifacts-by-branch-type db-state)
               (latest-transition-by-tags db-state)
               ]])

(defn api-routes
  "Utility, since swagger requires the routes as well as yada"
  [db-state]
  [(apic/ping-route)
   (apic/version-route)
   (reports db-state)
   (artifact db-state)
   (transition db-state)])


(defn app
  [db-state]
  (-> ["" (api-routes db-state)]
      (y/swaggered
       {:info {:title "Build Manager"
               :version "1.0"
               :contact {:name "DevOps",
                         :url ""},
               :description "Tracks builds and their status"}
        :basePath "/api"})
      (b/tag :org.restivo.build-manager.api/v1)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment
  

  )
