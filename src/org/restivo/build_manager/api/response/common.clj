(ns org.restivo.build-manager.api.response.common
  "Functions for converting db data to api responses and vice versa."
  (:require
   ;; TODO: slamhound these deps; there is a lot of uselesss cruft in it
   [clojure.string :as str]
   [utilza.log :as ulog]
   [clojure.edn :as edn]
   [clojure.java.io :as jio]
   [ring.mock.request :as req]
   [mount.core :as mount]
   [clojure.test :as t]
   [org.restivo.build-manager.timestamp :as ts]
   [org.restivo.build-manager.webutils :as wutil]
   [yada.context :as ctx]
   [bidi.bidi :as b]
   [org.restivo.build-manager.model :as m]
   [org.restivo.build-manager.db.queries :as q]
   [org.restivo.build-manager.db :as db]
   [utilza.repl :as urepl]
   [schema.core :as s]
   [datomic.api :as d]
   [utilza.java :as ujava]
   [clojure.walk :as walk]
   [utilza.misc :as umisc]
   [yada.schema :as ys]
   [bidi.schema :as bschem]
   [utilza.core :as utilza]
   [cheshire.core :as json]
   [org.restivo.build-manager.util :as u]
   [bidi.ring :as br]
   [byte-streams :as bs]
   [yada.yada :as y]
   [org.restivo.build-manager.log :as l]
   [ring.util.response :as res]
   [taoensso.timbre :as log]
   ))



;; TODO spec it for types
(defn json-or-csv
  "Takes a context and a result clojure data structure (map, seq, etc).
   Returns a string in either json or csv basec on the content-type header.
   Technically yada does type coercion, so this shouldn't be necessary, 
   but it doesn't seem to know about CSV, so we're manually hacking this here."
  [ctx res]
  (let [t (ctx/content-type ctx)]
    (case t
      "application/json" res
      "text/csv" (u/->csv res)
      (do (log/warn "unknown context type" t)
          res))))





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment



  
  )




