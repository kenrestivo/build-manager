(ns org.restivo.build-manager.api.response.v2
  "Functions for converting db data to api responses and vice versa."
  (:require
   ;; TODO: slamhound these deps; there is a lot of uselesss cruft in it
   [clojure.string :as str]
   [utilza.log :as ulog]
   [clojure.edn :as edn]
   [clojure.java.io :as jio]
   [ring.mock.request :as req]
   [mount.core :as mount]
   [clojure.test :as t]
   [org.restivo.build-manager.api.response.common :as resp]
   [org.restivo.build-manager.db.queries.v2 :as v2]
   [org.restivo.build-manager.timestamp :as ts]
   [org.restivo.build-manager.webutils :as wutil]
   [yada.context :as ctx]
   [bidi.bidi :as b]
   [org.restivo.build-manager.model :as m]
   [org.restivo.build-manager.db.queries :as q]
   [org.restivo.build-manager.db :as db]
   [utilza.repl :as urepl]
   [schema.core :as s]
   [datomic.api :as d]
   [utilza.java :as ujava]
   [clojure.walk :as walk]
   [utilza.misc :as umisc]
   [yada.schema :as ys]
   [datomic.api :as d]
   [bidi.schema :as bschem]
   [utilza.core :as utilza]
   [utilza.datomic :as dutils]
   [cheshire.core :as json]
   [org.restivo.build-manager.util :as u]
   [bidi.ring :as br]
   [byte-streams :as bs]
   [yada.yada :as y]
   [org.restivo.build-manager.log :as l]
   [ring.util.response :as res]
   [taoensso.timbre :as log]
   ))

(defmulti ->name
  "Convert a db-ident or a keyword to an un-namespaced string"
  type)

(defmethod ->name  clojure.lang.PersistentArrayMap
  [m]
  (log/trace m)
  ;; Handle ns'ed keys
  (or (some-> m :db/ident ->name)
      ;; also, the key may have been un-ns'ed before getting here. *sigh*
      (some-> m :ident ->name)))


(defmethod ->name  clojure.lang.Keyword
  [k]
  #_(log/trace k)
  (-> k utilza/un-ns-key name))


(defmethod ->name  :default
  [k]
  ;; Scream.
  (log/error "unimplemented ->name conversion for" (type k) k))


(def key-map-datomic->web
  {:build-timestamp ts/->timestamp
   :type ->name 
   :id str
   :timestamp ts/->timestamp})





;; TODO: spec it for types
(defn datomic->web
  "Utility function. Takes a key-map-datomic->web, context, and sequence of values from datomic
   Returns a proper response based on context, ready for yada/response.
   If not supplied a context, doesn't complect this with json/csv conversion (better for unit tests)"
  [key-map m]
  (->> (dissoc m :db/id)
       (utilza/xform-keys utilza/un-ns-key)
       (umisc/munge-columns key-map)))


(defn datomic-map->web
  "Given a key map and a thing which may be a map,
   walks the whole tree of the thing, and converts any maps within from datomic map to a json-ready map 
   with formatting for all values based on keys and transform functions in key-map"
  [key-map m]
  (walk/prewalk (fn [x]
                  #_(log/trace (type x) "type" x)
                  (cond
                    (map? x) (datomic->web key-map x)
                    ;;; datomic entity is not only not a map, but if you run map? on it, it crashes the compiler!
                    (= datomic.query.EntityMap (type x)) (->> x
                                                              (into {})
                                                              (datomic->web key-map))
                    true x))
                m))


(defn datomic-maps->web
  "Takes a key-map of keys and transform functions, and a seq of maps
   Returns datomic-map->web on all of those "
  [key-map ms]
  (map (partial datomic-map->web key-map)  ms))


(defn ctx->environment
  "Takes a db state and yada/ring context.
   Returns JSON string with results."
  [db-state ctx]
  (let [{:keys [path query]} (:parameters ctx)
        {:keys [env-name]} path
        {:keys [date]} query
        db (if date
             (db/as-of   db-state date)
             (db/latest db-state))]
    (log/trace "web call: get environment" env-name date)
    (or (some->> env-name
                 (dutils/one-kv db  :environment/name)
                 (d/entity db)
                 d/touch
                 (datomic-map->web key-map-datomic->web)
                 (resp/json-or-csv ctx))
        (throw (ex-info "404 not found" {:status 404
                                         :error "Not found"})))))



(defn ctx->environment-artifacts
  "Takes a db state and yada/ring context.
   Returns JSON string with results."
  [db-state ctx]
  (let [{:keys [path query]} (:parameters ctx)
        {:keys [env-name]} path
        {:keys [date]} query
        db (if date
             (db/as-of  db-state date)
             (db/latest db-state))]
    (log/trace "web call: get environment" env-name db)
    (or (some->> env-name
                 (dutils/one-kv db  :environment/name)
                 (d/entity db)
                 d/touch
                 :environment/artifacts
                 (datomic-map->web key-map-datomic->web)
                 (resp/json-or-csv ctx)) 
        (throw (ex-info "404 not found" {:status 404
                                         :error "Not found"})))))


(defn ctx->environment-artifact
  "Takes a db state and yada/ring context.
   Returns JSON string with results."
  [db-state ctx]
  (let [{:keys [path query]} (:parameters ctx)
        {:keys [env-name short-hash]} path
        {:keys [date]} query
        db (if date
             (db/as-of   db-state date)
             (db/latest db-state))]
    (log/trace "web call: get environment" env-name)
    (or (some->> env-name
                 (dutils/one-kv db  :environment/name)
                 (d/entity db)
                 d/touch
                 ;;; XXX goofy, best to just write a datomic datalog query rather than this filtering TODO: fix.
                 :environment/artifacts
                 (filter #(-> % :artifact/short-hash (= short-hash)))
                 (datomic-map->web key-map-datomic->web)
                 (resp/json-or-csv ctx))
        (throw (ex-info "404 not found" {:status 404
                                         :error "Not found"})))))

(defn ctx->transition
  "Takes a db state and yada/ring context.
   Returns JSON string with results."
  [db-state ctx]
  (let [id (get-in ctx [:parameters :path :id])
        db (db/latest db-state)]
    (log/trace "web call: get transition" id)
    (or (some->> id
                 java.util.UUID/fromString
                 (dutils/one-kv db  :transition/id)
                 (d/entity db)
                 d/touch
                 (datomic-map->web key-map-datomic->web)
                 (resp/json-or-csv ctx))
        (throw (ex-info "404 not found" {:status 404
                                         :error "Not found"})))))

(defn ctx->artifact
  "Takes a db state and yada/ring context.
   Returns JSON string with results."
  [db-state ctx]
  (let [short-hash (get-in ctx [:parameters :path :short-hash])
        db (db/latest db-state)]
    (log/trace "web call: get artifact" short-hash)
    (or (some->> short-hash
                 (dutils/one-kv db  :artifact/short-hash)
                 (d/entity db)
                 d/touch
                 (datomic-map->web key-map-datomic->web)
                 (resp/json-or-csv ctx))
        (throw (ex-info "404 not found" {:status 404
                                         :error "Not found"})))))

(defn ctx->freeform-result
  "Takes a db-state and a yada/ring context.
   Returns a JSON string with the results."
  [db-state ctx]
  (log/trace "web call: freeform query")
  (let [db (db/latest db-state)
        {:keys [datalog vars]}  (get-in ctx [:parameters :body])]
    ;; XXX this haky paramter passing tho
    (some->> (map edn/read-string vars)
             (filter identity)
             (apply d/q (edn/read-string datalog) db)
             (datomic-maps->web key-map-datomic->web)
             (resp/json-or-csv ctx))))


(defn ctx->latest-artifact-by-tags-name-branch-type
  "Takes a db-state and a yada/ring context.
   Returns a map of the results."
  [db-state ctx]
  (log/trace "web call: latest artifacts branch type response")
  (let [db (db/latest db-state)
        {:keys [tags artifact-name git-branch artifact-type]} (-> ctx :parameters :body)]
    ;; XXX mangling the key to namespace it here feeels REALLY REALLY dirty. should be in model coercion?
    (some->> (q/artifact-name-branch-tag-ids->latest-artifact db
                                                              artifact-name
                                                              (utilza/ns-key "artifact.type" artifact-type)
                                                              git-branch tags)
             (datomic-map->web key-map-datomic->web)
             (resp/json-or-csv ctx))))



(defn transact!
  "Takes a connection and a valid seq of txes in datomic format.
  Writes to db, and throws a 406 if problem"
  [conn data]
  (log/trace "Transacting to datomic:" data)
  (if-let [r (some->> data (d/transact conn) deref) ]
    (do (log/debug "saved, got returned" r)
        r)
    (do
      (log/error "the transaction did not save")
      (throw (ex-info "Record would not save" {:status 406
                                               :record data})))))

;; TODO: spec this fn for types
(defn insert!
  "A simple CRUDdy save. Takes db-state, a text namespace for entity keys, and a yada ctx.
   Returns posted if successful."
  [{:keys [conn] :as db-state} ns ctx]
  (let [{{:keys [body]} :parameters} ctx]
    (log/trace "web call: saving" body)
    (let [tx (-> db-state
                 db/latest
                 (m/crud-map->tx ns body)
                 vector)]
      (log/debug "about to send this to datomic:" tx)
      (->> tx
           (transact! conn)
           m/get-what-was-inserted
           (datomic-map->web key-map-datomic->web)
           ;;; XXX hack
           json/encode)))) ;; XXX remove this-- yada coerces to json automatically

(defn un-boilerplate
  "This thing was cut/pasted and it made me sad. Refactored out"
  [db-after env-name]
  (some->> env-name
           (dutils/one-kv db-after  :environment/name)
           (d/entity db-after)
           d/touch
           (datomic-map->web key-map-datomic->web)
           ;; XX should not be necessary,  yada is supposed to coerce
           json/encode))

(defn mangle-artifact-env!
  [{:keys [conn] :as db-state} k ctx]
  (let [{:keys [env-name short-hash]} (-> ctx :parameters :path)
        db (db/latest db-state)]
    (log/trace (format "web call: %s %s from %s" k short-hash env-name))
    (let [tx [[k [:environment/name env-name ] :environment/artifacts [:artifact/short-hash short-hash]]]
          {:keys [db-after]} (transact! conn tx)]
      (un-boilerplate db-after env-name))))



(defn overwrite-artifact-name!
  [{:keys [conn] :as db-state} {{{:keys [env-name artifact-name short-hash]} :path} :parameters}]
  (let [db (db/latest db-state)
        to-remove (q/artifacts-by-name-in-env db env-name artifact-name)]
    (as->  to-remove x
      (set x)
      (disj x short-hash) ;; just remove duplicates and make everyone's life easier
      (q/remove-and-replace-env-tx env-name
                                   artifact-name
                                   x
                                   short-hash)
      (transact! conn x)
      (:db-after x)
      (un-boilerplate x env-name))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment


  (log/set-level! :trace)
  

  (require '[clojure.tools.trace] )
  
  (clojure.tools.trace/untrace-vars #'org.restivo.build-manager.model/get-what-was-inserted)

  (clojure.tools.trace/trace-vars #'org.restivo.build-manager.api.response.v2/datomic-maps->web)
  (clojure.tools.trace/trace-vars #'org.restivo.build-manager.api.response.v2/datomic-map->web)
  
  (clojure.tools.trace/trace-vars #'org.restivo.build-manager.api.response.v2/transact!)
  
  
  )
