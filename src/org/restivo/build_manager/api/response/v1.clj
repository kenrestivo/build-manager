(ns org.restivo.build-manager.api.response.v1
  "Functions for converting db data to api responses and vice versa."
  (:require
   ;; TODO: slamhound these deps; there is a lot of uselesss cruft in it
   [clojure.string :as str]
   [utilza.log :as ulog]
   [clojure.edn :as edn]
   [clojure.java.io :as jio]
   [ring.mock.request :as req]
   [mount.core :as mount]
   [clojure.test :as t]
   [org.restivo.build-manager.api.response.common :as resp]
   [org.restivo.build-manager.api.response.v1 :as v1]
   [org.restivo.build-manager.timestamp :as ts]
   [org.restivo.build-manager.webutils :as wutil]
   [yada.context :as ctx]
   [bidi.bidi :as b]
   [org.restivo.build-manager.model :as m]
   [org.restivo.build-manager.db.queries :as q]
   [org.restivo.build-manager.db :as db]
   [utilza.repl :as urepl]
   [schema.core :as s]
   [datomic.api :as d]
   [utilza.java :as ujava]
   [clojure.walk :as walk]
   [utilza.misc :as umisc]
   [yada.schema :as ys]
   [bidi.schema :as bschem]
   [utilza.core :as utilza]
   [cheshire.core :as json]
   [org.restivo.build-manager.util :as u]
   [bidi.ring :as br]
   [byte-streams :as bs]
   [yada.yada :as y]
   [org.restivo.build-manager.log :as l]
   [ring.util.response :as res]
   [taoensso.timbre :as log]
   ))


(def key-map-datomic->web
  {:build-timestamp ts/->timestamp
   :type (comp name utilza/un-ns-key :db/ident) ;; ugly, but works
   :id str
   :timestamp ts/->timestamp})


;; TODO: spec it for types
(defn datomic-map->web
  "Utility function. Takes a key-map-datomic->web, context, and sequence of values from datomic
   Returns a proper response based on context, ready for yada/response.
   If not supplied a context, doesn't complect this with json/csv conversion (better for unit tests)"
  [key-map m]
  (->> m
       (utilza/xform-keys utilza/un-ns-key)
       (umisc/munge-columns key-map)))

(defn datomic-maps->web
  ([key-map ms]
   (walk/prewalk (fn [x]
                   (cond
                     (map? x) (datomic-map->web key-map x)
                     true x))
                 ms))
  ([key-map ctx ms]
   (->> ms
        (datomic-maps->web key-map))))



(defn ctx->artifacts-by-date
  "Takes a db-state and a yada/ring context.
   Returns a map of the results."
  [db-state ctx]
  (log/trace "web call: artifacts by date")
  ;; TODO XXX WTF?
  {})


(defn ctx->latest-artifacts-by-branch-type
  "Takes a db-state and a yada/ring context.
   Returns a map of the results."
  [db-state ctx]
  (log/trace "web call: latest artifacts branch type response")
  (let [db (db/latest db-state)
        {:artifact/keys [branch-name type] :as dat}  (->> [:parameters :query]
                                                          (get-in ctx )
                                                          (m/->datomic db "artifact"))]
    (log/trace "as datomic:" dat)
    (some->> (q/branch-type->latest-artifacts db branch-name type)
             (datomic-maps->web v1/key-map-datomic->web)
             (resp/json-or-csv ctx))))


(defn ctx->latest-transition-by-tags
  "Takes a db-state and a yada/ring context.
   Returns a map of the results."
  [db-state ctx]
  (log/trace "web call: latest transitions by tags")
  (let [db (db/latest db-state)
        tags (get-in ctx [:parameters :body :tags])]
    (some->> (q/tags->most-recent-transition db tags)
             (datomic-maps->web v1/key-map-datomic->web)
             (resp/json-or-csv ctx))))



(defn transact!
  "Takes a connection and a valid seq of txes in datomic format.
  Writes to db, and throws a 406 if problem"
  [conn data]
  (if-let [r (d/transact conn data)]
    (do
      (deref r)
      ;; TODO: return the id/uuid of the inserted item if present!
      "posted\n\n")
    (do
      (log/error "the transaction did not save")
      (throw (ex-info "Record would not save" {:status 406
                                               :record data})))))

;; TODO: spec this fn for types
(defn insert!
  "A simple CRUDdy save. Takes db-state, a text namespace for entity keys, and a yada ctx.
   Returns posted if successful."
  [{:keys [conn] :as db-state} ns {{:keys [body]} :parameters}]
  (log/trace "web call: saving" body)
  (let [tx (-> db-state
               db/latest
               (m/crud-map->tx ns body)
               vector)]
    (log/debug "about to send this to datomic:" tx)
    (transact! conn tx)))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment



  
  )




