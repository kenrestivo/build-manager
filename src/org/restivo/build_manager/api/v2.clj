(ns org.restivo.build-manager.api.v2
  (:require 
   [clojure.string :as str]
   [clojure.java.io :as jio]
   [ring.mock.request :as req]
   [mount.core :as mount]
   [clojure.test :as t]
   [org.restivo.build-manager.api.common :as apic]
   [org.restivo.build-manager.api.v1 :as apiv1]
   [org.restivo.build-manager.timestamp :as ts]
   [org.restivo.build-manager.webutils :as wutil]
   [org.restivo.build-manager.api.response.v1 :as v1]
   [org.restivo.build-manager.api.response.v2 :as v2]
   [org.restivo.build-manager.api.response.common :as resp]
   [yada.context :as ctx]
   [bidi.bidi :as b]
   [org.restivo.build-manager.model :as m]
   [org.restivo.build-manager.db.queries :as q]
   [org.restivo.build-manager.db :as db]
   [utilza.repl :as urepl]
   [schema.core :as s]
   [datomic.api :as d]
   [utilza.java :as ujava]
   [utilza.misc :as umisc]
   [yada.schema :as ys]
   [utilza.log :as ulog]
   [bidi.schema :as bschem]
   [utilza.core :as utilza]
   [cheshire.core :as json]
   [org.restivo.build-manager.util :as u]
   [bidi.ring :as br]
   [byte-streams :as bs]
   [yada.yada :as y]
   [org.restivo.build-manager.log :as l]
   [ring.util.response :as res]
   [taoensso.timbre :as log]
   ))


(defn freeform-datalog-query
  [db-state]
  ["/freeform-datalog-query"
   (y/resource
    {:responses wutil/standard-responses
     :swagger/tags ["reports"]
     :methods {:post {:summary "Search based on a datalog query and variables"
                      :description "Provide a datalog query string, and any parameters including reader macros. i.e. dates must be provided as parameters i.e. #inst \"2017-03-31T16:29:23.000-00:00\", and id's as #uuid \"xxxx-xxxx-xxxx-xxxx\""
                      :parameters {:body {:datalog String 
                                          :vars [String]}}
                      :consumes [{:media-type #{"application/json"}
                                  :charset "UTF-8"}]
                      :produces [{:media-type "application/json"}
                                 {:media-type "text/csv"}]
                      :response #(v2/ctx->freeform-result db-state %)}}})])



(defn artifact
  "Routing for the artifact record operations"
  [db-state]
  ["/artifact" [["" (y/resource
                     {:summary "An artifact, such as a Docker image, a JAR file, a tarball, etc"
                      :responses wutil/standard-responses
                      :methods {:post {:summary "Insert data for an artifact"
                                       :description (str "Create a new artifact record: "
                                                         ;; XXX hideous hack
                                                         ;; need cleaner way to show schemas in a readable format
                                                         m/Artifact)
                                       :parameters {:body m/Artifact}
                                       :consumes [{:media-type #{"application/json"}
                                                   :charset "UTF-8"}]
                                       :response (partial v2/insert! db-state "artifact")}}})]
                [["/" :short-hash] (y/resource
                                    {:methods {:get {:parameters {:path {:short-hash m/HexHash}}
                                                     :summary "Data about an individual artifact."
                                                     :description "Data about a particular artifact. Accepts a short hash identifying an artifact. Returns the artifact details."
                                                     :produces [{:media-type "application/json"}]
                                                     :response #(v2/ctx->artifact db-state %)}}
                                     :responses wutil/standard-responses})]]])




(defn transition
  "Routing for the transition record operations"
  [db-state]
  ["/transition" [["" (y/resource
                       {:summary "A group of artifacts which transitioned together from one state to another."
                        :methods {:post {:summary "Insert data for a transition run. Artifacts are identified by short-hash'es"
                                         :description (str "Create a new transition record. "
                                                           "Returns the transition record as added to DB, with its ID included."
                                                           ;; XXX hideous hack
                                                           ;; need cleaner way to show schemas in a readable format
                                                           m/TransitionPost)
                                         :parameters {:body m/TransitionPost}
                                         :consumes [{:media-type #{"application/json"}
                                                     :charset "UTF-8"}]
                                         :response (partial v2/insert! db-state "transition")}}
                        :responses wutil/standard-responses})]
                  [["/" :id] (y/resource
                              {:parameters {:path {:id m/UUIDString}} 
                               :id :org.restivo.build-manager.transition/get-by-id
                               :methods {:get {:summary "Data about an individual transition."
                                               :description "Data about a particular transition for a set of artifacts. Accepts a UUID identifying a transition run. Returns the transition details including its artifacts."
                                               :produces [{:media-type "application/json"}]
                                               :response #(v2/ctx->transition db-state %)}}
                               :responses wutil/standard-responses})]] ])


(defn environment
  "Routing for the environment record operations"
  [db-state]
  ["/environment" [[""
                    (y/resource
                     {:methods {:post {:summary "Create a new environment. Artifacts are identified by short-hash'es. Upserts are supported: you can post all or part of what is deployed in the environment."
                                       :description (str "Create a new environment record. "
                                                         "Returns the environment record as added to DB, with its ID included."
                                                         ;; XXX hideous hack
                                                         ;; need cleaner way to show schemas in a readable format
                                                         m/EnvironmentPost)
                                       :parameters {:body m/EnvironmentPost}
                                       :consumes [{:media-type #{"application/json"}
                                                   :charset "UTF-8"}]
                                       :response (partial v2/insert! db-state "environment")}}
                      :responses wutil/standard-responses})]
                   [["/" :env-name]
                    (y/resource
                     {:parameters {:path {:env-name m/NotEmptyStr}
                                   :query {(s/optional-key :date)  m/LongDate}} 
                      :id :org.restivo.build-manager.environment/get-by-name
                      :summary "A deployed environment, and data regarding was deployed to it and when."
                      :methods {
                                ;; :put TODO: copy post, mostly, should work, i hope
                                ;; :patch, TODO to add stuff! i.e. tags or artifacts?
                                ;; :delete TODO: why not
                                :get {:summary "Data about an individual environment."
                                      :description "Data about a particular environment for a set of artifacts. Accepts a name. Returns the environment details including its artifacts."
                                      :produces [{:media-type "application/json"}]
                                      :response #(v2/ctx->environment db-state %)}}
                      :responses wutil/standard-responses})]
                   ;; not too userul, only post is really helpful
                   [["/" :env-name "/artifacts"]
                    (y/resource
                     {:parameters {:path {:env-name m/NotEmptyStr}
                                   :query {(s/optional-key :date) m/LongDate}}
                      :summary "A group of artifacts that are/were deployed to an environment."
                      :id :org.restivo.build-manager.environment/artifacts
                      :methods {
                                ;; TODO: put too.
                                ;; :patch {:parameters {:body (s/both [m/ShortHash]
                                ;;                                    (s/pred (comp pos? count)) )}
                                ;;         :consumes [{:media-type #{"application/json"}
                                ;;                     :charset "UTF-8"}]
                                ;;         :response #(log/info %)} ;; TODO implement
                                :get {:summary "List of Artifacts in an environment."
                                      :description "Artifacts in an environment. Accepts a name."
                                      :produces [{:media-type "application/json"}]
                                      :response #(v2/ctx->environment-artifacts db-state %)}}
                      :responses wutil/standard-responses})]
                   ;; TODO: copy/paste this and do the same for tags?
                   [["/" :env-name "/artifacts/" :short-hash]
                    (y/resource
                     {:id :org.restivo.build-manager.environment/short-hash 
                      :methods {:delete {:summary "Remove this artifact from this environment"
                                         :parameters {}
                                         :consumes []
                                         :response (partial v2/mangle-artifact-env! db-state :db/retract)}
                                :put {:summary "Add this artifact to this environment"
                                      :parameters {:path {:env-name m/NotEmptyStr
                                                          :short-hash m/HexHash}}
                                      :consumes [] ;; ??? anything bueller? blank is ok? no body anyway.
                                      :response  (partial v2/mangle-artifact-env! db-state :db/add)}
                                :get {:parameters {:path {:env-name m/NotEmptyStr
                                                          :short-hash m/HexHash}
                                                   :query {(s/optional-key :date) m/LongDate}}
                                      :summary "Get an artifact in an environment."
                                      :description "Artifact in an environment. Accepts an environment name and a short-hash of the artifact."
                                      :produces [{:media-type "application/json"}]
                                      :response #(v2/ctx->environment-artifact db-state %)
                                      }}
                      :responses wutil/standard-responses})]
                   [["/" :env-name "/artifact/" :artifact-name "/" :short-hash]
                    (y/resource
                     {:id :org.restivo.build-manager.environment/short-hash 
                      :methods {:delete {:summary "Remove this artifact from this environment"
                                         :parameters {:path {:env-name m/NotEmptyStr
                                                             :artifact-name m/NotEmptyStr ;; Superfluous
                                                             :short-hash m/HexHash}}
                                         :consumes [] ;; ??? anything bueller? blank is ok? no body anyway.
                                         ;; NOTE: exactly the same as the non-artifact-name endpoint 
                                         :response (partial v2/mangle-artifact-env! db-state :db/retract)}
                                :post {:summary "Inserts a new, additional artifact with this name and short hash"
                                       :parameters {:path {:env-name m/NotEmptyStr
                                                           :artifact-name m/NotEmptyStr ;; superfluous
                                                           :short-hash m/HexHash}}
                                       :consumes [] ;; ??? anything bueller? blank is ok? no body anyway.
                                       ;; NOTE same as put without name!
                                       :response (partial v2/mangle-artifact-env! db-state :db/add)} 
                                :put {:summary "Overwrites the existing artifact with this name, with the artifact represented by short-hash"
                                      :parameters {:path {:env-name m/NotEmptyStr
                                                          :artifact-name m/NotEmptyStr
                                                          :short-hash m/HexHash}}
                                      :consumes [] ;; ??? anything bueller? blank is ok? no body anyway.
                                      :response  (partial v2/overwrite-artifact-name! db-state)}}
                      :responses wutil/standard-responses})]] ])




(defn latest-artifact-by-tags-name-branch
  [db-state]
  ["/latest-artifact-by-tags-name-type-branch"
   (y/resource
    {:responses wutil/standard-responses
     :swagger/tags ["reports"]
     :methods {:post {:summary "Get latest artifact of name, branch, and type given, to pass a transition with tags given"
                      :description (str "Gets latest artifact of tags and name and branch and type given")
                      :parameters {:body m/LatestArtifactByTagsNameBranchRequest}
                      :consumes [{:media-type #{"application/json"}
                                  :charset "UTF-8"}]
                      :produces [{:media-type "application/json"}
                                 {:media-type "text/csv"}]
                      :response  (partial v2/ctx->latest-artifact-by-tags-name-branch-type  db-state)}}})])

(defn reports
  [db-state]
  ["/reports" [(apiv1/latest-artifacts-by-branch-type db-state)
               (apiv1/latest-transition-by-tags db-state)
               (latest-artifact-by-tags-name-branch db-state)
               (freeform-datalog-query db-state)]])

(defn api-routes
  "Utility, since swagger requires the routes as well as yada"
  [db-state]
  ;; This idiotic "" is required becuause swagger.
  (-> [""  [(apic/ping-route)
            (apic/version-route)
            (reports db-state)
            (artifact db-state)
            (transition db-state)
            (environment db-state)]]
      (y/swaggered
       {:info {:title "Build Manager"
               :version "2.0"
               :contact {:name "DevOps",
                         :url ""},
               :description "Tracks builds and their status"}
        :basePath "/api/v2"})
      (b/tag :org.restivo.build-manager.api/v2)))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment



  
  )
