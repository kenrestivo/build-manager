(ns org.restivo.build-manager.log
  (:require [clojure.tools.trace :as trace]
            [mount.core :as mount]
            [io.aviso.exception :as aviso-ex]
            [taoensso.timbre.appenders.3rd-party.logstash :as ls]
            [io.aviso.repl :as pst]
            [clojure.string :as str]
            [taoensso.timbre :as log]
            [utilza.misc :as umisc]
            [taoensso.timbre.appenders.core :as appenders]
            [utilza.java :as ujava]))



(log/set-level! :error) ;; required to avoid breaking junit

(def group "org.restivo")
(def artifact "build-manager")

(defn readable-stacktrace
  [e]
  (binding [aviso-ex/*fonts* {}]
    (pst/pretty-pst (or (.getCause e) e))))

(defn welcome-log
  "Log welcome messages"
  [config]
  [(str "Welcome to Build Manager: " (ujava/revision-info group artifact))
   (str "Starting logging:\n" (with-out-str (clojure.pprint/pprint (umisc/redact-keys  config [:password :secret-key]))))])

(defn build-version
  "Utility to print the semantic version at run time"
  []
  (->> (ujava/get-project-properties group artifact)
       :version
       println))

(defn make-appenders
  [spit-filename logstash-host logstash-port]
  (cond-> {:println nil}
    logstash-host (assoc :logstash
                         (ls/logstash-appender logstash-host
                                               (or logstash-port 5000)
                                               {:flush? true
                                                :pr-stacktrace  readable-stacktrace}))
    spit-filename (assoc :spit
                         (appenders/spit-appender
                          {:fname spit-filename}))))

(defn start-logger
  [{:keys [log] :as config}]
  (let [{:keys [spit-filename logstash-host logstash-port level]} log]
    ;; to stdout first, for debugging
    (doseq [l (welcome-log config)]
      (println l))
    (log/merge-config! (merge log
                              {:level level
                               :output-fn (partial log/default-output-fn {:stacktrace-fonts {}})
                               ;; Explicitly disable (nil) the println so it doesn't duplicate messages
                               :appenders (make-appenders spit-filename logstash-host logstash-port)}))
    ;; TODO: make EXEC_ENV aware and use only in dev or qa
    (alter-var-root #'clojure.tools.trace/tracer (fn [_]
                                                   (fn [name value]
                                                     (log/debug name value))))
    ;; now log into logstash or wherever.
    (doseq [l (welcome-log config)]
      (log/info l))
    log)) ;; return it so it gets into the state



(mount/defstate log 
  :start (start-logger (mount/args))
  :stop (log/info "Shutting down logger"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(comment

  (log/error (Exception. "foobar"))
  (println (.getCause *e))


  (log/set-level! :info)


  )
