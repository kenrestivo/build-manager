(ns org.restivo.build-manager.api
  (:require 
   [clojure.string :as str]
   [clojure.java.io :as jio]
   [ring.mock.request :as req]
   [mount.core :as mount]
   [clojure.test :as t]
   [org.restivo.build-manager.timestamp :as ts]
   [org.restivo.build-manager.webutils :as wutil]
   [org.restivo.build-manager.api.common :as apic]
   [org.restivo.build-manager.api.v1 :as v1]
   [org.restivo.build-manager.api.v2 :as v2]
   [yada.context :as ctx]
   [bidi.bidi :as b]
   [org.restivo.build-manager.model :as m]
   [org.restivo.build-manager.db.queries :as q]
   [org.restivo.build-manager.db :as db]
   [utilza.repl :as urepl]
   [schema.core :as s]
   [datomic.api :as d]
   [utilza.java :as ujava]
   [utilza.misc :as umisc]
   [yada.schema :as ys]
   [bidi.schema :as bschem]
   [utilza.core :as utilza]
   [cheshire.core :as json]
   [org.restivo.build-manager.util :as u]
   [bidi.ring :as br]
   [byte-streams :as bs]
   [yada.yada :as y]
   [org.restivo.build-manager.log :as l]
   [ring.util.response :as res]
   [taoensso.timbre :as log]
   ))


(defonce debug-atom (atom nil))



(defn app
  "Returns the routes in bidi/yada format"
  [db-state]
  ["" [["/" (y/as-resource  "Hit the /api or /api/v2 endpoint.\n")] ;; TODO: redirect
       ;; (apic/status) ;; for testing, not sure how useful in prod (may also be security hole, remove?)
       ;; (apic/sources) ;; TODO: remove this security hole
       (apic/rest-parameters-test)
       (apic/throw-resource db-state)
       (apic/inspect-post db-state)
       ["/api/v2" (v2/api-routes db-state)] ;; note: order matters, and /api/v2 is a bit of a hack but works.
       ["/api" (v1/app db-state)]
       ["/foobar" (y/as-resource "whut")]
       [true (y/resource wutil/response-404)]
       ]])








;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  
  (log/error *e)
  

  (log/info "wtf")
  
  (some-> @debug-atom
          :parameters
          :body)
  
  (some-> @debug-atom
          :parameters
          :body
          (update-in  [:artifacts] (partial q/artifact-hashes->ids (db/latest db/db))))
  
  (some-> @debug-atom
          :properties
          :org.restivo.build-manager/hashes)




  
  
  )


