(ns org.restivo.build-manager.server
  (:require 
   [org.restivo.build-manager.api :as api]
   [org.restivo.build-manager.log :as l]
   [org.restivo.build-manager.db :as db] 
   [aleph.http :as a]
   [yada.yada :as y]
   [utilza.repl :as urepl]
   [mount.core :as mount]
   [taoensso.timbre :as log]
   ))


(defn start-server
  [settings db]
  (log/info "starting web server" settings)
  (some->> settings
           (y/listener (api/app db))))


(mount/defstate server
  :start  (some->  (mount/args) :server (start-server db/db))
  :stop (when-let [{:keys [close]} server]
          (log/info "shutting down webserver")
          (close)))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  ((-> server :foo :bar))

  (map? server)
  
  (-> server :foo :bar .call)

  (defn foo [] 1)

  (.call foo)
  
  (urepl/hjall foo)

  )
