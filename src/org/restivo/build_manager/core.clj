(ns org.restivo.build-manager.core
  "Main entry point"
  (:gen-class)
  (:require 
   [mount.core :as mount]
   [taoensso.timbre :as log]
   [utilza.log :as ulog]
   [org.restivo.build-manager.log :as db-log]
   [org.restivo.build-manager.nrepl :as n]
   [org.restivo.build-manager.db :as db]
   [org.restivo.build-manager.server :as srv]
   [org.restivo.build-manager.util :as u]
   [io.aviso.repl :as pst]
   [clojure.spec :as s] ;; XXX not used?
   [org.restivo.build-manager.conf :as conf]
   ))




;; TODO: fdef
(defn start!
  "Abstract the running away from main for easier testing (i.e. without sys/exit"
  [conf-file-arg command & args]
  (let [conf-file (or conf-file-arg "config.yml")]
    (println (format "Starting with conf file: %s\ncommand: %s\nargs %s" conf-file
                     command
                     args))
    ;; *sigh*
    (try
      (slurp conf-file-arg)
      (catch Throwable e
        (println (u/usage))
        (throw (ex-info "Not a valid conf file" {:conf-file conf-file-arg}))))
    (mount/start-with-args (conf/read-and-validate conf-file)
                           )))

(defn everything-running?
  "Takes a coll of mount states. Returns true if all the services passd in are running"
  [states]
  (every? identity (map map? states)))


(defn -main
  "Entry point for the daemon"
  [& [conf-file-arg command & args]]
  (try
    (start! conf-file-arg command args)
    (let [essential-services [db/db srv/server]]
      (when-not (everything-running? essential-services)
        (throw (ex-info "Services failed to start" {:states essential-services}))))
    @(promise) ;; wait forever for the thread to exit
    (System/exit 0)
    (catch Exception e
      (u/usage)
      (pst/pretty-pst e)
      (println (.getMessage e))
      (println (.getCause e))
      (System/exit 1))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  
  (ulog/catcher
   (mount/stop)
   (start! "resources/defaults/standard-debug.yml" nil) ) 
  

  (ulog/catcher
   (mount/stop)
   (start! "resources/defaults/conn-debug.yml" nil))



  (ulog/catcher
   (mount/stop)
   (start! "resources/defaults/nrepl-debug.yml" nil))



  (ulog/catcher
   (mount/stop)
   (start! "resources/defaults/alt-db-debug.yml" nil))


  (ulog/catcher
   (mount/stop)
   (start! "resources/defaults/logstash-test.yml" nil))


  
  )


(comment

  (everything-running? [db/db srv/server])

  (log/set-level! :trace)

  (log/debug "wtf")
  
  (log/error *e)

  
  )
