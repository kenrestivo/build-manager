FROM openjdk:8u151-jre-slim-stretch

WORKDIR /srv

ADD target/build-manager.jar /srv/build-manager.jar

EXPOSE 8089
EXPOSE 7777

ARG BUILD_NUMBER
ENV BUILD_NUMBER $BUILD_NUMBER

ARG GIT_BRANCH
ENV GIT_BRANCH $GIT_BRANCH

ARG GIT_COMMIT
ENV GIT_COMMIT $GIT_COMMIT

ARG REVISION
ENV REVISION $REVISION

LABEL BUILD_NUMBER $BUILD_NUMBER  \
GIT_BRANCH $GIT_BRANCH \
GIT_COMMIT $GIT_COMMIT  \
REVISION $REVISION


CMD ["java", "-jar", "/srv/build-manager.jar", "config.yml"]


