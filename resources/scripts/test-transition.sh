#!/bin/bash

D=transition.json

[ "$1" ] && D="$1"

curl -i \
    -X POST \
    --header 'Content-Type: application/json' \
    --header 'Accept: application/json' \
    -d @../test-data/${D} \
    http://localhost:8089/api/transition
