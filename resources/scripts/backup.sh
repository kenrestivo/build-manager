#!/bin/bash

# Backs up the datomic database
# Requires a /mnt/backups directory (or alternate dir, passed in as first arg on command line) with sufficient storage

BACKUPDIR=/mnt/backups

# override if necessary
[ "$1" ] && BACKUPDIR=$1

BACKUP=file:${BACKUPDIR}
DB="datomic:free://datomic-free:4334/build-manager"

OVERHEAD="docker \
	run \
	-i \
	--rm \
	--name datomic-backup \
	--log-driver syslog \
        --link datomic-free \
	-e ALT_HOST=datomic-backup \
	-v /tmp:/log \
	-v ${BACKUPDIR}:/backups \
	akiel/datomic-free:0.9.5544 \
	/datomic-free-0.9.5544/bin/datomic "

# TODO: restore is reverse, should be easy
$OVERHEAD backup-db ${DB} ${BACKUP}


$OVERHEAD list-backups file:/backups


## note: syncing to s3 is left to the platform-management version of this.

