#!/bin/bash

## TODO: this is a nasty copy/paste from backup. combine them!

# Backs up the datomic database
# Requires a /mnt/backups directory (or alternate dir, passed in as first arg on command line) with sufficient storage

BACKUPDIR=/mnt/backups

# override if necessary
[ "$1" ] && BACKUPDIR=$1


BACKUP=file:${BACKUPDIR}
DB="datomic:free://datomic-free:4334/build-manager"

OVERHEAD="docker \
	run \
	-i \
	--rm \
	--name datomic-restore \
	--log-driver syslog \
	-e ALT_HOST=datomic-backup \
        --link datomic-free \
	-v /tmp:/log \
	-v ${BACKUPDIR}:/backups \
	akiel/datomic-free:0.9.5544 \
	/datomic-free-0.9.5544/bin/datomic "

# TODO: restore is reverse, should be easy
$OVERHEAD restore-db  file:/backups ${DB} 

$OVERHEAD list-backups file:/backups




