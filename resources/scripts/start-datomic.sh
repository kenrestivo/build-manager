#!/bin/bash

docker \
    run \
    -d \
    -p 4334-4336:4334-4336 \
    --rm \
    --name datomic-free \
    --log-driver syslog \
    -e ALT_HOST=datomic-free \
    -v /home/src/builds-manager/datomic-data:/data \
    -v /tmp:/log \
    akiel/datomic-free:0.9.5544
