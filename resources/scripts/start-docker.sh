#!/bin/sh


GIT_BRANCH=`git rev-parse --abbrev-ref HEAD  | sed 's/\//./g' | tr '[:upper:]' '[:lower:]'`

IMG=build-manager

docker stop $IMG
docker rm $IMG

docker run \
       --log-driver syslog \
       -d \
       -v /home/src/builds-manager/resources/defaults/docker-debug.yml:/srv/config.yml \
       -p 7777:7777 \
       -p 8089:8089 \
       --name $IMG \
       --link datomic-free \
       ${IMG}:$GIT_BRANCH 
