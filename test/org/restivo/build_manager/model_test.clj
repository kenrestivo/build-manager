(ns org.restivo.build-manager.model-test
  (:require
   [clojure.test :refer :all]
   [clojure.string :as str]
   [clojure.java.io :as jio]
   [mount.core :as mount]
   [clojure.test :refer :all]
   [org.restivo.build-manager.model :refer :all]
   [utilza.repl :as urepl]
   [utilza.log :as ulog]
   [utilza.misc :as umisc]
   [clojure.spec :as spec]
   [org.restivo.build-manager.db-test :as dbt]
   [clojure.spec.gen :as gen]
   [datomic.api :as d]
   [cheshire.core :as json]
   [org.restivo.build-manager.util :as u]
   [org.restivo.build-manager.db :as db]
   [byte-streams :as bs]
   [taoensso.timbre :as log]
   ))



(defonce test-db (atom {}))

(defn with-db
  [f] 
  (dbt/with-db
    test-db
    (fn []
      ;;; any other setup required!
      (log/trace "other setup")
      (f))))

(use-fixtures :each with-db)

(deftest insert-id-test
  (is (= [:foo] (keys (insert-id {:foo 1} :artifact))))
  (is (= [:foo :transition/id] (keys (insert-id {:foo 1} :transition)))))





