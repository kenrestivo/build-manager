(ns org.restivo.build-manager.db-test
  (:require 
   [clojure.string :as str]
   [clojure.java.io :as jio]
   [utilza.log :as ulog]
   [ring.mock.request :as req]
   [mount.core :as mount]
   [clojure.edn :as edn]
   [clojure.test :refer :all]
   [org.restivo.build-manager.db :refer :all]
   [org.restivo.build-manager.model :as m]
   [org.restivo.build-manager.api.response.common :as resp]
   [utilza.repl :as urepl]
   [utilza.misc :as umisc]
   [clojure.spec :as spec]
   [utilza.datomic :as dutils]
   [clojure.spec.gen :as gen]
   [datomic.api :as d]
   [cheshire.core :as json]
   [org.restivo.build-manager.util :as u]
   [taoensso.timbre :as log]
   ))

(defonce test-db (atom {}))

;; redundant
(defn destroy-fake-db
  [fake-db]
  {:pre [(u/atom? fake-db)]}
  (swap! fake-db (fn [tdb]
                   (some-> tdb :conn d/release)
                   (some-> tdb :uri d/delete-database)))
  {})


(defn setup-fake-db
  "Takes an atom with a fake db map.
   Creates an in-memory db, and populates it with most recent schema"
  [fake-db]
  {:pre [(u/atom? fake-db)]}
  (swap! fake-db (fn [tdb]
                   (some-> tdb :conn d/release)
                   (some-> tdb :uri d/delete-database)
                   (start {:uri (format "datomic:mem://tdb-%s" (u/short-uuid))
                           :create? true}))))

;; TODO: what this really needs is an interceptor model like yada and pedestal
(defn with-db
  "Takes a function and a test db.
   Runs the function with the test db."
  [test-db f]
  {:pre [(u/atom? test-db)
         (fn? f)]}
  ;; wipe out any that already exist
  (try (destroy-fake-db test-db)
       (catch Throwable e
         nil))
  (setup-fake-db test-db)
  (let [res (f)]
    ;; awkward, but need to run the function then clean up, before returning the result
    (destroy-fake-db test-db)
    res))


(defn insert-json!
  [{:keys [conn] :as db-state} ns-str file-path]
  (some->> file-path
           slurp
           u/json-key-decode
           (m/crud-map->tx (latest db-state) ns-str)
           vector
           (d/transact conn)
           deref))


(defn generate-sample-artifacts
  "Returns count n of records ready for saving as edn and/or transacting into db"
  [n]
  (-> :org.restivo.build-manager/artifact
      spec/gen
      (gen/sample n)
      (#(map (partial m/->datomic "artifact") %))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest with-db-works
  (testing "with db... what is going on?")
  (is (= "works" (with-db test-db (constantly "works")))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  (->> 200
       generate-sample-artifacts
       (urepl/massive-spew "resources/test-data/sample-artifacts.edn"))

  (ulog/catcher
   (dutils/load-from-file "resources/test-data/sample-artifacts.edn" (:conn db)))


  (ulog/catcher
   (with-db test-db  #(log/info "wtf")))

  (ulog/catcher
   (setup-fake-db test-db))

  (ulog/catcher
   (destroy-fake-db test-db))


  (ulog/catcher
   (run-tests))
  
  
  )
