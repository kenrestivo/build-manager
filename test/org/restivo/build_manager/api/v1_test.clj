(ns org.restivo.build-manager.api.v1-test
  (:require
   [clojure.test :refer :all]
   [clojure.walk :as walk]
   [clojure.edn :as edn]
   [clojure.string :as str]
   [clojure.java.io :as jio]
   [utilza.log :as ulog]
   [ring.mock.request :as mock]
   [org.restivo.build-manager.api.common-test :as ctest]
   [mount.core :as mount]
   [clojure.test :refer :all]
   [org.restivo.build-manager.api :refer :all]
   [clojure-csv.core :as csv]
   [bidi.bidi :as b]
   [org.restivo.build-manager.model :as m]
   [org.restivo.build-manager.db :as db]
   [org.restivo.build-manager.db-test :as dbt]
   [org.restivo.build-manager.db.queries :as q]
   [utilza.repl :as urepl]
   [clojure.spec :as spec]
   [clojure.spec.gen :as gen]
   [datomic.api :as d]
   [utilza.datomic :as dutil]
   [yada.schema :as ys]
   [bidi.schema :as bschem]
   [cheshire.core :as json]
   [org.restivo.build-manager.util :as u]
   [bidi.ring :as br]
   [byte-streams :as bs]
   [yada.yada :as y]
   [ring.util.response :as res]
   [taoensso.timbre :as log]
   ))


(use-fixtures :each ctest/with-db)

(defn post-artifact-mock
  [test-db filename]
  (some-> (mock/request :post "/api/artifact" (-> filename slurp))
          (mock/content-type "application/json")
          (ctest/test-get-everything (-> test-db :handler))
          :status))

(deftest post-artifact-endpoint
  (is (= 200 (post-artifact-mock @ctest/test-db "resources/test-data/artifact.json")))
  (is   (= {:artifact/short-hash "85678ae90ed2"
            :artifact/name "rabbitmq"
            :artifact/type :artifact.type/docker
            :artifact/git-commit "9d70b06"
            :artifact/git-branch "develop"
            :artifact/build-number 47
            :artifact/build-timestamp #inst "2017-03-31T16:29:35.000-00:00"
            :artifact/semantic-version "0.1.12-bogus"}
           (-> @ctest/test-db
               db/latest
               (dutil/all-kv  :artifact/short-hash  "85678ae90ed2")
               (#(into {} %))
               (dissoc :db/id)))))



(deftest post-helm-endpoint
  (is (= 200 (post-artifact-mock @ctest/test-db "resources/test-data/helm-artifact.json")))
  (is   (= {:artifact/short-hash "85678ae90ed3"
            :artifact/name "rabbitmq"
            :artifact/type :artifact.type/helm-chart
            :artifact/git-commit "9d70b06"
            :artifact/git-branch "develop"
            :artifact/build-number 47
            :artifact/build-timestamp #inst "2018-03-31T16:29:36.000-00:00"
            :artifact/semantic-version "0.1.12-bogus"}
           (-> @ctest/test-db
               db/latest
               (dutil/all-kv  :artifact/short-hash "85678ae90ed3") 
               (#(into {} %))
               (dissoc :db/id)))))

(deftest post-archive-endpoint
  (is (= 200 (post-artifact-mock @ctest/test-db "resources/test-data/archive-artifact.json")))
  (is   (= {:artifact/short-hash "85678ae90ed4"
            :artifact/name "rabbitmq"
            :artifact/type :artifact.type/archive
            :artifact/git-commit "9d70b06"
            :artifact/git-branch "develop"
            :artifact/build-number 47
            :artifact/build-timestamp #inst "2018-03-31T16:29:35.000-00:00"
            :artifact/semantic-version "0.1.12-bogus"}
           (-> @ctest/test-db
               db/latest
               (dutil/all-kv  :artifact/short-hash "85678ae90ed4")
               (#(into {} %))
               (dissoc :db/id)))))

(deftest get-artifacts-json
  (dbt/insert-json! @ctest/test-db "artifact" "resources/test-data/artifact.json")
  (testing "valid request, valid result")
  (is (= '({:short-hash "85678ae90ed2",
            :name "rabbitmq",
            :type "docker",
            :git-commit "9d70b06",
            :git-branch "develop",
            :semantic-version "0.1.12-bogus",
            :build-number 47,
            :build-timestamp 20170331162935})
         (some-> (mock/request :get "/api/reports/latest-artifacts-by-branch-type" {:branch-name "develop"
                                                                                    :type "docker"})
                 (mock/header "Accept" "application/json")
                 (ctest/test-get-body (-> @ctest/test-db :handler))
                 (json/decode true))))
  (testing "bad request, per JEDI-8543. ")
  ;; TODO: XXX should actually return 404 but this is OK for now.
  (is (= 500 (some-> (mock/request :get "/api/reports/latest-artifacts-by-branch-type" {:branch-name "develop"
                                                                                        :type "asdf"})
                     (mock/header "Accept" "application/json")
                     (ctest/test-get-everything (-> @ctest/test-db :handler))
                     :status))))


(deftest get-versionless-artifacts-json
  (testing "Making sure you can insert and retrieve an artifact record without a semantic version")
  (dbt/insert-json! @ctest/test-db "artifact" "resources/test-data/artifact-versionless.json")
  (is (= '({:short-hash "85678ae90ed9",
            :name "rabbitmq",
            :type "docker",
            :git-commit "9d70b06",
            :git-branch "develop",
            :build-number 47,
            :build-timestamp 20170331162935})
         (some-> (mock/request :get "/api/reports/latest-artifacts-by-branch-type" {:branch-name "develop"
                                                                                    :type "docker"})
                 (mock/header "Accept" "application/json")
                 (ctest/test-get-body (-> @ctest/test-db :handler))
                 (json/decode true)
                 ))))

(deftest get-artifacts-csv
  (dbt/insert-json! @ctest/test-db "artifact" "resources/test-data/artifact.json")
  (let [res {"short-hash" "85678ae90ed2",
             "name" "rabbitmq",
             "type" "docker",
             "git-commit" "9d70b06",
             "semantic-version" "0.1.12-bogus",
             "git-branch" "develop",
             "build-number" "47",
             "build-timestamp" "20170331162935"}]
    (is (=  res
            (some->>
             (some-> (mock/request :get "/api/reports/latest-artifacts-by-branch-type" {:branch-name "develop"
                                                                                        :type "docker"})
                     (mock/header "Accept" "text/csv")
                     (ctest/test-get-body (-> @ctest/test-db :handler)))
             csv/parse-csv
             ;; note, the apply zipmap hack will work only with one.
             ;; when writing tests for multiple, will need utilza columnize etc.
             (apply zipmap))))))



(defn post-transition-mock
  [test-db file-path]
  (some-> (mock/request :post "/api/transition" (-> file-path slurp))
          (mock/content-type "application/json")
          (ctest/test-get-everything (-> test-db :handler))
          :status))


(deftest post-transition-test
  ;; fixture
  (dbt/insert-json! @ctest/test-db "artifact" "resources/test-data/artifact.json")
  (testing "good data")
  (is (= 200 (post-transition-mock @ctest/test-db "resources/test-data/transition.json")))
  (testing "bad data")
  (is (= 400 (post-transition-mock @ctest/test-db "resources/test-data/bad-transition.json")))
  ;; TODO: 
  ;;(dutil/find-all-by-attr (db/latest @ctest/test-db) :transition/timestamp)
  ;; (q/find-tag (db/latest @ctest/test-db) "release" "test-release")
  )

(deftest get-transition-search
  ;; use fixtures?
  (dbt/insert-json! @ctest/test-db "artifact" "resources/test-data/artifact.json")
  (dbt/insert-json! @ctest/test-db "transition" "resources/test-data/transition.json")
  (testing "correct query")
  (let [values {:tags [{:key "release" :value "test-release"}]}
        to-web {:artifacts ;; can't guarantee order, so set not vector
                #{{:short-hash "85678ae90ed2",
                   :name "rabbitmq",
                   :type "docker",
                   :git-commit "9d70b06",
                   :git-branch "develop",
                   :semantic-version "0.1.12-bogus",
                   :build-number 47,
                   :build-timestamp 20170331162935}},
                :timestamp 20170331162923,
                :tags ;; can't guarantee order, so set not vector
                #{{:key "release", :value "test-release"}
                  {:key "stage", :value "regression-pass"}}}]
    ;; annoying, but necessary. use fixtures?
    (dbt/insert-json! @ctest/test-db "artifact" "resources/test-data/artifact.json")
    (dbt/insert-json! @ctest/test-db "transition" "resources/test-data/transition.json")
    (is (= to-web
           (u/nested-colls->sets
            (some-> (mock/request :post "/api/reports/latest-transition-by-tags" (json/encode values))
                    (mock/content-type "application/json")
                    (mock/header "Accept" "application/json")
                    (ctest/test-get-body (-> @ctest/test-db :handler))
                    (json/decode true)
                    ;; have to remove id as it is going to be different every time
                    (dissoc :id))))))
  (testing "and not or")
  (let [values {:tags [{:key "release" :value "test-release"}
                       {:key "not-here" :value "missing"}]}]
    ;; annoying, but necessary. use fixtures?
    (dbt/insert-json! @ctest/test-db "artifact" "resources/test-data/artifact.json")
    (dbt/insert-json! @ctest/test-db "transition" "resources/test-data/transition.json")
    (is (empty?
         (u/nested-colls->sets
          (some-> (mock/request :post "/api/reports/latest-transition-by-tags" (json/encode values))
                  (mock/content-type "application/json")
                  (mock/header "Accept" "application/json")
                  (ctest/test-get-body (-> @ctest/test-db :handler))
                  (json/decode true)
                  ;; have to remove id as it is going to be different every time
                  (dissoc :id)))))))


(deftest errors-400-ish
  (testing "bad data type")
  (let [res (let [values {:tags [{:key "release" :value "test-release"}
                                 {:key "not-here" :value "missing"}]}]
              (some-> (mock/request :post "/api/reports/latest-transition-by-tags" (json/encode values))
                      ;; notice missing content type header!!
                      (mock/header "Accept" "application/json")
                      (ctest/test-get-everything (-> @ctest/test-db :handler))))]
    (is (= {"error" "Unsupported Media Type"
            "debug" "Unsupported Media Type"}
           (-> res ctest/get-body json/decode)))
    (is (= 415 (-> res :status)))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  (ulog/catcher
   (run-tests))



  (ulog/spewer
   (dutil/find-all-by-attr (db/latest @ctest/test-db) :artifact/build-timestamp))






  
  (log/info :wtf)
  

  (ulog/catcher
   )

  
  )



