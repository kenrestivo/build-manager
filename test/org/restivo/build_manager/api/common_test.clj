(ns org.restivo.build-manager.api.common-test
  (:require
   [clojure.test :refer :all]
   [clojure.walk :as walk]
   [clojure.edn :as edn]
   [clojure.string :as str]
   [clojure.java.io :as jio]
   [utilza.log :as ulog]
   [ring.mock.request :as mock]
   [mount.core :as mount]
   [clojure.test :refer :all]
   [org.restivo.build-manager.api :refer :all]
   [clojure-csv.core :as csv]
   [bidi.bidi :as b]
   [org.restivo.build-manager.model :as m]
   [org.restivo.build-manager.db :as db]
   [org.restivo.build-manager.db-test :as dbt]
   [org.restivo.build-manager.db.queries :as q]
   [utilza.repl :as urepl]
   [clojure.spec :as spec]
   [clojure.spec.gen :as gen]
   [datomic.api :as d]
   [utilza.datomic :as dutil]
   [yada.schema :as ys]
   [bidi.schema :as bschem]
   [cheshire.core :as json]
   [org.restivo.build-manager.util :as u]
   [bidi.ring :as br]
   [byte-streams :as bs]
   [yada.yada :as y]
   [ring.util.response :as res]
   [taoensso.timbre :as log]
   ))

(defonce test-db (atom {}))



(defn setup-fake-db
  "This is the handler-specific, yada-specific setup."
  [fake-db]
  {:pre [(u/atom? fake-db)]}
  (swap! fake-db (fn [state]
                   (assoc state :handler (br/make-handler (app state))))))

(defn with-db
  [f] 
  (dbt/with-db test-db
    (fn []
      ;; run the yada setup after the db setup which runs in dbt/with-db
      (setup-fake-db test-db)
      ;; turn off all logging, it's annoying
      ;; TODO: maybe have a custom appender that saves it to an atom, and check that the message actually logged. that'd be slick.
      (log/with-config {}
        (f)))))


(use-fixtures :each with-db)



(defn test-get-everything
  ([m h]
   (some->> m
            h
            deref)))

(defn get-body
  [m]
  (some-> m
          :body
          bs/to-string))

(defn test-get-body
  [m h]
  (some-> m
          (test-get-everything  h)
          get-body))





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(deftest api-endpoints
  (is (= 200 (-> (mock/request :get "/api/swagger.json")
                 (test-get-everything (-> @test-db :handler) )
                 :status)))
  (is (= "Pong\n" (-> (mock/request :get "/api/ping")
                      (test-get-body (-> @test-db :handler)))))
  (is (= 200 (-> (mock/request :get "/api/version")
                 (test-get-everything (-> @test-db :handler))
                 :status))))



(deftest nowhere
  (is (= 404 (-> (mock/request :get "/nothing")
                 (test-get-everything (-> @test-db :handler))
                 :status )))
  (is (= 404 (-> (mock/request :get "/api/nothing")
                 (test-get-everything (-> @test-db :handler))
                 :status )))
  (is (= {"error" "404"}
         (-> (mock/request :get "/nothing")
             (test-get-everything (-> @test-db :handler))
             get-body
             json/decode))))


(deftest homepage
  (is (= 200 (-> (mock/request :get "/")
                 (test-get-everything (-> @test-db :handler))
                 :status ))))


(deftest throw
  (let [res (-> (mock/request :get "/throw")
                (test-get-everything (-> @test-db :handler)))]
    (is (= 500 (:status res)))
    (is (= {"error" "Error. See logs."}
           (-> res
               get-body
               json/decode)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  (ulog/catcher
   (dbt/setup-fake-db test-db)
   (setup-fake-db test-db)
   (dbt/insert-json! @test-db "artifact" "resources/test-data/artifact.json")
   (dbt/insert-json! @test-db "transition" "resources/test-data/transition.json"))

  
  (log/set-level! :info)
  (log/set-level! :trace)
  
  (log/info "wtf")
  
  (ulog/catcher
   (run-tests)) 


  (ulog/spewer
   (app @test-db))

  (ulog/catcher
   (with-db get-artifacts-by-branch-name-json))

  (ulog/catcher
   (with-db get-artifacts-json))

  (log/info "wtf2")

  (ulog/catcher
   (-> (mock/request :get "/nothing")
       (test-get-everything (-> @test-db :handler))
       get-body
       json/decode))

  (ulog/catcher
   (-> (mock/request :get "/throw")
       (test-get-everything (-> @test-db :handler))
       get-body
       (json/decode true) 
       ))
  
  (require 'clojure.tools.trace)  
  (clojure.tools.trace/untrace-ns 'org.restivo.build-manager.api.response)



  (clojure.tools.trace/untrace-ns 'yada.yada)


  (clojure.tools.trace/untrace-ns 'org.restivo.build-manager.db.queries))



(comment
  ;;; work and'd make nice unit tests maybe
  
  (ulog/spewer
   (b/match-route (app debug-atom) "/path-vals/wtf"))

  (ulog/spewer
   (b/match-route (app debug-atom) "/path-vals"))
  
  (ulog/spewer
   (b/match-route (app debug-atom) "/")) 

  (ulog/spewer
   (b/match-route (app debug-atom) "/status"))
  )

(comment
  ;; these all work

  (ulog/spewer
   (let [res (-> (mock/request :get "/path-vals/wtf")
                 (mock/header "Accept" "application/json")
                 (test-get-everything (-> @test-db :handler)))]
     (log/trace "wtf")
     [(:status res)
      (-> res get-body json/decode)]))

  
  ;; TODO; cover with test, but can't be too picky since vals will be dynamic
  (ulog/spewer
   (let [res (-> (mock/request :get "/status")
                 (test-get-everything (-> @test-db :handler)))]
     [(:status res)
      (-> res get-body json/decode)]))


  ;; works!
  (ulog/spewer
   (let [res (-> (mock/request :get "/sources/project.clj")
                 (test-get-everything (-> @test-db :handler)))]
     (log/trace "wtf")
     [(:status res)
      (-> res get-body )]))

  
  ;; works!!
  (ulog/spewer
   (let [res (-> (mock/request :get "/")
                 (test-get-everything (-> @test-db :handler)))]
     (log/trace "wtf")
     [(:status res)
      (-> res get-body )]))

  ;; works
  (ulog/spewer
   (let [res (-> (mock/request :get "/throw")
                 (test-get-everything (-> @test-db :handler)))]
     [(:status res)
      (-> res get-body json/decode)]))



  
  
  )




