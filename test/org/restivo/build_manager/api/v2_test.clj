(ns org.restivo.build-manager.api.v2-test
  (:require
   [clojure.test :refer :all]
   [clojure.walk :as walk]
   [clojure.edn :as edn]
   [clj-time.coerce :as tc]
   [clojure.string :as str]
   [clojure.java.io :as jio]
   [utilza.log :as ulog]
   [ring.mock.request :as mock]
   [mount.core :as mount]
   [clojure.test :refer :all]
   [org.restivo.build-manager.api :refer :all]
   [org.restivo.build-manager.api.common-test :as ctest]
   [clojure-csv.core :as csv]
   [bidi.bidi :as b]
   [org.restivo.build-manager.model :as m]
   [org.restivo.build-manager.db :as db]
   [org.restivo.build-manager.db-test :as dbt]
   [org.restivo.build-manager.db.queries :as q]
   [utilza.repl :as urepl]
   [clojure.spec :as spec]
   [clojure.spec.gen :as gen]
   [datomic.api :as d]
   [utilza.datomic :as dutils]
   [yada.schema :as ys]
   [org.restivo.build-manager.api.response.v2 :as v2]
   [bidi.schema :as bschem]
   [cheshire.core :as json]
   [org.restivo.build-manager.util :as u]
   [bidi.ring :as br]
   [byte-streams :as bs]
   [yada.yada :as y]
   [ring.util.response :as res]
   [taoensso.timbre :as log]
   ))


(use-fixtures :each ctest/with-db)


(deftest routing-and-methods
  (testing "Making sure all the endpoint routing works, all the methods required are supported in yada/bidi")
  (let [a (app debug-atom)]
    (doseq [[endpoint exp] [["/api/v2/environment"  #{:post}]
                            ["/api/v2/environment/foo/artifacts/blah" #{:get :delete :put}]
                            ["/api/v2/environment/foo/artifact/bar/baz" #{:delete :put :post} ]
                            ["/api/v2/artifact" #{:post}]
                            ["/api/v2/artifact/foo" #{:get}]
                            ["/api/v2/transition" #{:post}]
                            ["/api/v2/transition/wtf" #{:get}]
                            ["/api/v2/path-vals/wtf" #{:get}]
                            ]]
      (testing endpoint)
      (is (= (->> endpoint
                  (b/match-route a)
                  u/handler-method-keys-set)
             exp)))))

(deftest get-freeform-search
  (dbt/insert-json! @ctest/test-db "artifact" "resources/test-data/artifact.json")
  (dbt/insert-json! @ctest/test-db "transition" "resources/test-data/transition.json")
  (testing "freeform query with parameters")
  (is (string? (let [vals {:datalog "[:find  ?tid
         :in $ ?ts
         :where
         [?t :transition/timestamp ?ts]
         [?t :transition/id ?tid]
         ]"
                           :vars  ["#inst \"2017-03-31T16:29:23.000-00:00\""]}]
                 (some-> (mock/request :post "/api/v2/reports/freeform-datalog-query" (json/encode vals))
                         (mock/content-type "application/json")
                         (mock/header "Accept" "application/json")
                         (ctest/test-get-body (-> @ctest/test-db :handler))
                         (json/decode true)
                         ffirst)))) 
  (testing "freeform query without parameters")
  (is (string? (let [vals {:datalog "[:find  ?tid
         :in $ 
         :where
         [?t :transition/timestamp ?ts]
         [?t :transition/id ?tid]
         ]"
                           :vars  []}]
                 (some-> (mock/request :post "/api/v2/reports/freeform-datalog-query" (json/encode vals))
                         (mock/content-type "application/json")
                         (mock/header "Accept" "application/json")
                         (ctest/test-get-body (-> @ctest/test-db :handler))
                         (json/decode true)
                         ffirst))))
  (is (= '(["85678ae90ed2"])
         (let [vals {:datalog "[:find  ?ah
         :in $ ?ts
         :where
         [?t :transition/timestamp ?ts]
         [?t :transition/artifacts ?as]
         [?as :artifact/short-hash ?ah]
         ]"
                     :vars  ["#inst \"2017-03-31T16:29:23.000-00:00\""]}]
           (some-> (mock/request :post "/api/v2/reports/freeform-datalog-query" (json/encode vals))
                   (mock/content-type "application/json")
                   (mock/header "Accept" "application/json")
                   (ctest/test-get-body (-> @ctest/test-db :handler))
                   (json/decode true)))))

  )


(defn post-artifact-mock
  [test-db filename]
  (some-> (mock/request :post "/api/v2/artifact" (-> filename slurp))
          (mock/content-type "application/json")
          (ctest/test-get-everything (-> test-db :handler))))


(deftest post-artifact-endpoint
  (let [res (post-artifact-mock @ctest/test-db "resources/test-data/artifact.json")]
    (is (= 200 (:status res)))
    (is (= {"short-hash" "85678ae90ed2",
            "name" "rabbitmq",
            "type" "docker",
            "git-commit" "9d70b06",
            "git-branch" "develop",
            "build-number" 47,
            "build-timestamp" 20170331162935,
            "semantic-version" "0.1.12-bogus"}
           (some-> res
                   ctest/get-body
                   json/decode))))
  (is   (= {:artifact/short-hash "85678ae90ed2"
            :artifact/name "rabbitmq"
            :artifact/type :artifact.type/docker
            :artifact/git-commit "9d70b06"
            :artifact/git-branch "develop"
            :artifact/build-number 47
            :artifact/build-timestamp #inst "2017-03-31T16:29:35.000-00:00"
            :artifact/semantic-version "0.1.12-bogus"}
           (-> @ctest/test-db
               db/latest
               (dutils/all-kv  :artifact/short-hash  "85678ae90ed2")
               (#(into {} %))
               (dissoc :db/id)))))


(defn post-transition-mock
  [test-db file-path]
  (some-> (mock/request :post "/api/v2/transition" (-> file-path slurp))
          (mock/content-type "application/json")
          (ctest/test-get-everything (-> test-db :handler))))



(deftest post-transition-test
  ;; fixture
  (dbt/insert-json! @ctest/test-db "artifact" "resources/test-data/artifact.json")
  (testing "good data")
  (let [res (post-transition-mock @ctest/test-db "resources/test-data/transition.json")]
    (is (= 200 (:status res)))
    (is (= {"artifacts"
            [{"short-hash" "85678ae90ed2",
              "name" "rabbitmq",
              "type" "docker",
              "git-commit" "9d70b06",
              "git-branch" "develop",
              "build-number" 47,
              "build-timestamp" 20170331162935,
              "semantic-version" "0.1.12-bogus"}],
            "timestamp" 20170331162923,
            "tags"
            [{"key" "release", "value" "test-release"}
             {"key" "stage", "value" "regression-pass"}]}
           (-> res
               ctest/get-body
               json/decode
               (dissoc "id")))))
  
  (testing "bad data")
  (is (= 400 (->> "resources/test-data/bad-transition.json"
                  (post-transition-mock @ctest/test-db )
                  :status ) ))
  ;; TODO: 
  ;;(dutils/find-all-by-attr (db/latest @ctest/test-db) :transition/timestamp)
  ;; (q/find-tag (db/latest @ctest/test-db) "release" "test-release")
  
  )

(deftest get-transition-test
  (testing "valid uuid test")
  (dbt/insert-json! @ctest/test-db "artifact" "resources/test-data/artifact.json")
  (dbt/insert-json! @ctest/test-db "transition" "resources/test-data/transition.json")
  (is (=  (let [db (db/latest @ctest/test-db)
                id (->> :transition/id
                        (dutils/find-by-key db )
                        ffirst
                        (d/entity db)
                        d/touch
                        :transition/id
                        str)]
            ;; uut
            (some-> (mock/request :get (str "/api/v2/transition/" id ) )
                    (mock/header "Accept" "application/json")
                    (ctest/test-get-body (-> @ctest/test-db :handler))
                    (json/decode true)
                    (dissoc :id)))
          ;; expected
          {:artifacts
           [{:short-hash "85678ae90ed2",
             :name "rabbitmq",
             :type "docker",
             :git-commit "9d70b06",
             :git-branch "develop",
             :build-number 47,
             :build-timestamp 20170331162935,
             :semantic-version "0.1.12-bogus"}],
           :timestamp 20170331162923,
           :tags
           [{:key "release", :value "test-release"}
            {:key "stage", :value "regression-pass"}]}))
  (testing "404 not found")
  (let [id "5ac3f3b9-6562-4e90-99de-8886347e54b5"]
    (is (= (some-> (mock/request :get (str "/api/v2/transition/" id ) )
                   (mock/header "Accept" "application/json")
                   (ctest/test-get-everything (-> @ctest/test-db :handler))
                   :status)
           404)) )
  (testing "illegal uuid test")
  (let [id "lolhahahanotauuid"]
    (is (= (some-> (mock/request :get (str "/api/v2/transition/" id ) )
                   (mock/header "Accept" "application/json")
                   (ctest/test-get-everything (-> @ctest/test-db :handler))
                   :status)
           400))))


(defn post-environment-mock
  [test-db file-path]
  (some-> (mock/request :post "/api/v2/environment" (-> file-path slurp))
          (mock/content-type "application/json")
          (ctest/test-get-everything (-> test-db :handler))))


(deftest post-environment-test
  ;; fixture
  (dbt/insert-json! @ctest/test-db "artifact" "resources/test-data/artifact.json")
  (testing "good data")
  (let [res (post-environment-mock @ctest/test-db "resources/test-data/environment.json")]
    (is (= 200 (:status res)))
    (is (= {"name" "staging-1",
            "artifacts"
            [{"short-hash" "85678ae90ed2",
              "name" "rabbitmq",
              "type" "docker",
              "git-commit" "9d70b06",
              "git-branch" "develop",
              "build-number" 47,
              "build-timestamp" 20170331162935,
              "semantic-version" "0.1.12-bogus"}],
            "tags" [{"key" "type", "value" "staging"}]}
           (-> res
               ctest/get-body
               json/decode
               (dissoc "id")))))
  
  ;; TODO: 
  ;;(dutils/find-all-by-attr (db/latest @ctest/test-db) :environment/timestamp)
  ;; (q/find-tag (db/latest @ctest/test-db) "release" "test-release")
  
  )





(deftest get-environment-test
  (testing "valid uuid test")
  (dbt/insert-json! @ctest/test-db "artifact" "resources/test-data/artifact.json")
  (dbt/insert-json! @ctest/test-db "environment" "resources/test-data/environment.json")
  (is (= {:name "staging-1",
          :artifacts
          [{:short-hash "85678ae90ed2",
            :name "rabbitmq",
            :type "docker",
            :git-commit "9d70b06",
            :git-branch "develop",
            :build-number  47,
            :build-timestamp 20170331162935,
            :semantic-version "0.1.12-bogus"}],
          :tags [{:key "type", :value "staging"}]}
         (let [db (db/latest @ctest/test-db)
               env-name "staging-1"]
           ;; uut
           (some-> (mock/request :get (str "/api/v2/environment/" env-name ) )
                   (mock/header "Accept" "application/json")
                   (ctest/test-get-body (-> @ctest/test-db :handler))
                   (json/decode true)
                   (dissoc :id)))))
  (testing "404 not found")
  (is (= 404
         (let [env-name "nothing"]
           (some-> (mock/request :get (str "/api/v2/environment/" env-name ) )
                   (mock/header "Accept" "application/json")
                   (ctest/test-get-everything (-> @ctest/test-db :handler))
                   :status)))))

(deftest environment-artifacts
  (dbt/insert-json! @ctest/test-db "artifact" "resources/test-data/artifact.json")
  (dbt/insert-json! @ctest/test-db "environment" "resources/test-data/environment.json")
  (let [env-name "staging-1"]
    (is (= #{{:short-hash "85678ae90ed2",
              :name "rabbitmq",
              :type "docker",
              :git-commit "9d70b06",
              :git-branch "develop",
              :build-number 47,
              :build-timestamp 20170331162935,
              :semantic-version "0.1.12-bogus"}}
           ;; uut
           (some-> (mock/request :get (str "/api/v2/environment/" env-name  "/artifacts") )
                   (mock/header "Accept" "application/json")
                   (ctest/test-get-everything (-> @ctest/test-db :handler))
                   :body ;; XXX why no have to json decode? that's weird
                   ))) ))


(deftest get-one-artifact-in-env
  (dbt/insert-json! @ctest/test-db "artifact" "resources/test-data/artifact.json")
  (dbt/insert-json! @ctest/test-db "environment" "resources/test-data/environment.json")
  (is (= (let [env-name "staging-1"
               short-hash "85678ae90ed2"]
           ;; uut
           (some-> (mock/request :get (str "/api/v2/environment/" env-name  "/artifacts/" short-hash) )
                   (mock/header "Accept" "application/json")
                   (ctest/test-get-everything (-> @ctest/test-db :handler))
                   ctest/get-body
                   (json/decode true)))
         ;; expected
         '({:short-hash "85678ae90ed2",
            :name "rabbitmq",
            :type "docker",
            :git-commit "9d70b06",
            :git-branch "develop",
            :build-number 47,
            :build-timestamp 20170331162935,
            :semantic-version "0.1.12-bogus"}))))



(deftest delete-artifact-from-env
  (dbt/insert-json! @ctest/test-db "artifact" "resources/test-data/artifact.json")
  (dbt/insert-json! @ctest/test-db "environment" "resources/test-data/environment.json")
  
  (testing "make sure the env got entered first, and the artifact is there")
  (let  [env-name "staging-1"
         artifact-name "rabbitmq"
         short-hash "85678ae90ed2" ]
    (is (= {:name "staging-1",
            :artifacts
            #{{:short-hash "85678ae90ed2",
               :name "rabbitmq",
               :type "docker",
               :git-commit "9d70b06",
               :git-branch "develop",
               :build-number 47,
               :build-timestamp 20170331162935,
               :semantic-version "0.1.12-bogus"}},
            :tags #{{:key "type", :value "staging"}}}
           (let [db (db/latest @ctest/test-db)]
             (some->> env-name
                      (dutils/one-kv db  :environment/name)
                      (d/entity db)
                      d/touch
                      (v2/datomic-map->web v2/key-map-datomic->web)))))
    
    (testing "deleting the artifact from the environment")
    (let [res   (some-> (mock/request :delete (str "/api/v2/environment/" env-name  "/artifacts/" short-hash) )
                        (mock/header "Accept" "application/json")
                        ;; uut
                        (ctest/test-get-everything (-> @ctest/test-db :handler)))]
      (is (= {:name "staging-1", :tags [{:key "type", :value "staging"}]}
             (-> res
                 ctest/get-body
                 (json/decode true))))
      (is (= 200 (:status res)))
      
      (testing "making sure it really is gone from the environment")
      (let [db (db/latest @ctest/test-db)]
        (is (= {:name "staging-1", :tags #{{:key "type", :value "staging"}}}
               (some->> env-name
                        (dutils/one-kv db  :environment/name)
                        (d/entity db)
                        d/touch
                        (v2/datomic-map->web v2/key-map-datomic->web))))))

    
    ))


(deftest put-artifact-into-env
  (dbt/insert-json! @ctest/test-db "artifact" "resources/test-data/artifact.json")
  (dbt/insert-json! @ctest/test-db "environment" "resources/test-data/empty-env.json")
  (let [env-name "staging-1"
        short-hash "85678ae90ed2"]
    (testing "putting the artifact")
    (let [res (some-> (mock/request :put (str "/api/v2/environment/" env-name  "/artifacts/" short-hash) )
                      (mock/header "Accept" "application/json")
                      ;; uut
                      (ctest/test-get-everything (-> @ctest/test-db :handler)))]
      (is (= (:status res) 200 ))
      (is (= (-> res ctest/get-body (json/decode true))
             {:name "staging-1",
              :artifacts
              [{:short-hash "85678ae90ed2",
                :name "rabbitmq",
                :type "docker",
                :git-commit "9d70b06",
                :git-branch "develop",
                :build-number 47,
                :build-timestamp 20170331162935,
                :semantic-version "0.1.12-bogus"}],
              :tags [{:key "type", :value "staging"}]}             )))
    (testing "make sure the env got entered, and the artifact is there")
    (let  [db (db/latest @ctest/test-db)]
      (is (= (some->> env-name
                      (dutils/one-kv db  :environment/name)
                      (d/entity db)
                      d/touch
                      (v2/datomic-map->web v2/key-map-datomic->web))
             {:name "staging-1",
              :artifacts
              #{{:build-number 47,
                 :build-timestamp 20170331162935,
                 :git-branch "develop",
                 :git-commit "9d70b06",
                 :name "rabbitmq",
                 :semantic-version "0.1.12-bogus",
                 :short-hash "85678ae90ed2",
                 :type "docker"}},
              :tags #{{:key "type", :value "staging"}}}
             )))) )

(deftest latest-artifact-by-tags-name-type-branch-test
  (dbt/insert-json! @ctest/test-db "artifact" "resources/test-data/artifact.json")
  (dbt/insert-json! @ctest/test-db "transition" "resources/test-data/transition.json")
  (let [res (some-> (mock/request :post "/api/v2/reports/latest-artifact-by-tags-name-type-branch"
                                  (json/encode {:artifact-name "rabbitmq"
                                                :artifact-type "docker"
                                                :git-branch "develop"
                                                :tags [{:key "stage", :value "regression-pass"}]}) )
                    (mock/header "Accept" "application/json")
                    (mock/content-type "application/json")
                    ;; uut
                    (ctest/test-get-everything (-> @ctest/test-db :handler)))]
    (is (= (:status res) 200))
    (is (= (-> res ctest/get-body
               (json/decode true))
           ;; expected
           {:short-hash "85678ae90ed2",
            :name "rabbitmq",
            :git-commit "9d70b06",
            :git-branch "develop",
            :semantic-version "0.1.12-bogus",
            :build-timestamp 20170331162935,
            :build-number 47,
            :type "docker"}))))


(deftest get-one-artifact
  (dbt/insert-json! @ctest/test-db "artifact" "resources/test-data/artifact.json")
  (is (= (let [short-hash "85678ae90ed2"]
           ;; uut
           (some-> (mock/request :get (str "/api/v2/artifact/"  short-hash) )
                   (mock/header "Accept" "application/json")
                   (ctest/test-get-everything (-> @ctest/test-db :handler))
                   ctest/get-body
                   (json/decode true)))
         ;; expected
         {:short-hash "85678ae90ed2",
          :name "rabbitmq",
          :type "docker",
          :git-commit "9d70b06",
          :git-branch "develop",
          :build-number 47,
          :build-timestamp 20170331162935,
          :semantic-version "0.1.12-bogus"}
         )))




(deftest env-artifact-by-name-tests
  (dbt/insert-json! @ctest/test-db "artifact" "resources/test-data/artifact.json")
  (dbt/insert-json! @ctest/test-db "transition" "resources/test-data/transition.json")
  (dbt/insert-json! @ctest/test-db "environment" "resources/test-data/environment.json")
  
  (testing "deleting the artifact from the named endpoint of the environment")
  (let [env-name "staging-1"
        artifact-name "rabbitmq"
        short-hash "85678ae90ed2"
        res (some-> (mock/request :delete (str "/api/v2/environment/" env-name
                                               "/artifact/" artifact-name
                                               "/" short-hash) )
                    (mock/header "Accept" "application/json")
                    ;; uut
                    (ctest/test-get-everything (-> @ctest/test-db :handler)))]
    (is (= {:name env-name, :tags [{:key "type", :value "staging"}]}
           (-> res
               ctest/get-body
               (json/decode true))))
    (is (= 200 (:status res)))
    
    (testing "making sure it really is gone from the environment")
    (let [db (db/latest @ctest/test-db)]
      (is (= {:name env-name, :tags #{{:key "type", :value "staging"}}}
             (some->> env-name
                      (dutils/one-kv db  :environment/name)
                      (d/entity db)
                      d/touch
                      (v2/datomic-map->web v2/key-map-datomic->web)))))))




(deftest post-artifact-into-artifact-name-env
  (dbt/insert-json! @ctest/test-db "artifact" "resources/test-data/artifact.json")
  (dbt/insert-json! @ctest/test-db "environment" "resources/test-data/empty-env.json")
  (let [env-name "staging-1"
        artifact-name "rabbitmq"
        short-hash "85678ae90ed2"]
    (testing "putting the artifact")
    (let [res (some-> (mock/request :post (str "/api/v2/environment/" env-name
                                               "/artifact/" artifact-name
                                               "/"  short-hash) )
                      (mock/header "Accept" "application/json")
                      ;; uut
                      (ctest/test-get-everything (-> @ctest/test-db :handler)))]
      (is (= (:status res) 200 ))
      (is (= (-> res ctest/get-body (json/decode true))
             {:name "staging-1",
              :artifacts
              [{:short-hash "85678ae90ed2",
                :name "rabbitmq",
                :type "docker",
                :git-commit "9d70b06",
                :git-branch "develop",
                :build-number 47,
                :build-timestamp 20170331162935,
                :semantic-version "0.1.12-bogus"}],
              :tags [{:key "type", :value "staging"}]}             )))
    (testing "make sure the env got entered, and the artifact is there")
    (let  [db (db/latest @ctest/test-db)]
      (is (= (some->> env-name
                      (dutils/one-kv db  :environment/name)
                      (d/entity db)
                      d/touch
                      (v2/datomic-map->web v2/key-map-datomic->web))
             {:name "staging-1",
              :artifacts
              #{{:build-number 47,
                 :build-timestamp 20170331162935,
                 :git-branch "develop",
                 :git-commit "9d70b06",
                 :name "rabbitmq",
                 :semantic-version "0.1.12-bogus",
                 :short-hash "85678ae90ed2",
                 :type "docker"}},
              :tags #{{:key "type", :value "staging"}}}
             )))) )


(deftest replace-artifact-in-env
  (dbt/insert-json! @ctest/test-db "artifact" "resources/test-data/artifact.json")
  (dbt/insert-json! @ctest/test-db "transition" "resources/test-data/transition.json")
  (dbt/insert-json! @ctest/test-db "artifact" "resources/test-data/dupe-artifact.json")
  (dbt/insert-json! @ctest/test-db "environment" "resources/test-data/environment.json")
  (let [short-hash "22278ae37ed3"
        env-name "staging-1"
        artifact-name "rabbitmq"
        res (some-> (mock/request :put (str "/api/v2/environment/" env-name
                                            "/artifact/" artifact-name
                                            "/"  short-hash) )
                    (mock/header "Accept" "application/json")
                    ;; uut
                    (ctest/test-get-everything (:handler @ctest/test-db)))]
    (is (= (:status res)
           200))
    (is (= (-> res
               ctest/get-body
               (json/decode true))
           {:name "staging-1",
            :artifacts
            [{:short-hash "22278ae37ed3",
              :name "rabbitmq",
              :type "docker",
              :git-commit "9d70b06",
              :git-branch "develop",
              :build-number 212,
              :build-timestamp 20180430163935,
              :semantic-version "0.1.12-bogus"}],
            :tags [{:key "type", :value "staging"}]}))
    (testing "Replacing with what's already there. Should not care")
    (is (= (some-> (mock/request :put (str "/api/v2/environment/" env-name
                                           "/artifact/" artifact-name
                                           "/"  short-hash) )
                   (mock/header "Accept" "application/json")
                   ;; uut
                   (ctest/test-get-everything (:handler @ctest/test-db))
                   :status)
           200))))


(deftest get-env-with-date
  (dbt/insert-json! @ctest/test-db "artifact" "resources/test-data/artifact.json")
  (dbt/insert-json! @ctest/test-db "transition" "resources/test-data/transition.json")
  (dbt/insert-json! @ctest/test-db "artifact" "resources/test-data/dupe-artifact.json")
  (dbt/insert-json! @ctest/test-db "environment" "resources/test-data/environment.json")
  (let [db (db/latest @ctest/test-db)
        env-name "staging-1"]
    ;; uut
    (is (= (some-> (mock/request :get (str "/api/v2/environment/" env-name "?date=" (u/now-long)) )
                   (mock/header "Accept" "application/json")
                   (ctest/test-get-body (-> @ctest/test-db :handler))
                   (json/decode true)
                   (dissoc :id))
           {:name "staging-1",
            :artifacts
            [{:short-hash "85678ae90ed2",
              :name "rabbitmq",
              :type "docker",
              :git-commit "9d70b06",
              :git-branch "develop",
              :build-number 47,
              :build-timestamp 20170331162935,
              :semantic-version "0.1.12-bogus"}],
            :tags [{:key "type", :value "staging"}]}))))


  (deftest environment-artifacts-with-date
  (dbt/insert-json! @ctest/test-db "artifact" "resources/test-data/artifact.json")
  (dbt/insert-json! @ctest/test-db "environment" "resources/test-data/environment.json")
  (let [env-name "staging-1"]
    (is (= #{{:short-hash "85678ae90ed2",
              :name "rabbitmq",
              :type "docker",
              :git-commit "9d70b06",
              :git-branch "develop",
              :build-number 47,
              :build-timestamp 20170331162935,
              :semantic-version "0.1.12-bogus"}}
           ;; uut
           (some-> (mock/request :get (str "/api/v2/environment/" env-name  "/artifacts" "?date=" (u/now-long)) )
                   (mock/header "Accept" "application/json")
                   (ctest/test-get-everything (-> @ctest/test-db :handler))
                   :body ;; XXX why no have to json decode? that's weird
                   ))) ))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  (ulog/catcher
   (run-tests))

  (log/set-level! :trace)

  (log/info :wtf)

  (ulog/catcher
   (dbt/setup-fake-db ctest/test-db)
   (ctest/setup-fake-db ctest/test-db)
   (dbt/insert-json! @ctest/test-db "artifact" "resources/test-data/artifact.json")
   (dbt/insert-json! @ctest/test-db "transition" "resources/test-data/transition.json")
   (dbt/insert-json! @ctest/test-db "artifact" "resources/test-data/dupe-artifact.json")
   (dbt/insert-json! @ctest/test-db "environment" "resources/test-data/environment.json"))




  

  


  )


(comment

  
  ;; works
  (ulog/spewer
   (-> (b/match-route (app debug-atom) "/route" )
       u/handler-method-keys-set))

  
  )
