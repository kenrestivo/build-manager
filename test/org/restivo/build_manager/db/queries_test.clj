(ns org.restivo.build-manager.db.queries-test
  (:require
   [clojure.test :refer :all]
   [utilza.log :as ulog]
   [datomic.api :as d]
   [org.restivo.build-manager.timestamp :as ts]
   [clojure.walk :as walk]
   [org.restivo.build-manager.db.queries :as q]
   [clojure.edn :as edn]
   [clojure.string :as str]
   [org.restivo.build-manager.model :as m]
   [clojure.java.io :as jio]
   [org.restivo.build-manager.db-test :as dbt]
   [mount.core :as mount]
   [clojure.test :refer :all]
   [org.restivo.build-manager.db.queries :refer :all]
   [utilza.repl :as urepl]
   [utilza.misc :as umisc]
   [clojure.spec :as spec]
   [clojure.spec.gen :as gen]
   [datomic.api :as d]
   [cheshire.core :as json]
   [org.restivo.build-manager.util :as u]
   [org.restivo.build-manager.db :as db]
   [byte-streams :as bs]
   [taoensso.timbre :as log]
   ))


(defonce test-db (atom {}))

(defn with-db
  [f] 
  (dbt/with-db test-db f))


(use-fixtures :each with-db)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UNIT TESTS
(deftest with-db-works
  (testing "with db... what is going on?")
  (is (= "works" (with-db (constantly "works")))))



(deftest find-tags-test
  (testing "find tags")
  (dbt/insert-json! @test-db "artifact" "resources/test-data/artifact.json")
  (dbt/insert-json! @test-db "transition" "resources/test-data/transition.json")
  
  (is (number? (find-tag (db/latest @test-db) "release" "test-release")))
  (is (= 1 (count (find-tags (db/latest @test-db) [{:key "release" :value "test-release"}]))))


  (testing "non existent tag")
  (let [db (db/latest @test-db)
        tags [{:key "release" :value "test-release"}
              {:key "not-here" :value "missing"}]]
    (is (empty? (find-tags db tags)))))



(deftest transition-test
  (dbt/insert-json! @test-db "artifact" "resources/test-data/artifact.json")
  (dbt/insert-json! @test-db "transition" "resources/test-data/transition.json")
  (testing "transition report")
  (let [res #:transition{:timestamp #inst "2017-03-31T16:29:23.000-00:00",
                         :tags
                         [#:tag{:key "release", :value "test-release"}
                          #:tag{:key "stage", :value "regression-pass"}],
                         :artifacts
                         [#:artifact{:short-hash "85678ae90ed2",
                                     :name "rabbitmq",
                                     :git-commit "9d70b06",
                                     :git-branch "develop",
                                     :semantic-version "0.1.12-bogus",
                                     :build-timestamp
                                     #inst "2017-03-31T16:29:35.000-00:00",
                                     :build-number 47,
                                     :type #:db{:ident :artifact.type/docker}}]}
        tags [{:key "release" :value "test-release"}]
        db (db/latest @test-db)]
    (is (= res (-> (tags->most-recent-transition db tags)
                   (dissoc :transition/id))))))


(deftest artifact-get-test
  (dbt/insert-json! @test-db "artifact" "resources/test-data/artifact.json")
  (let [res [#:artifact{:short-hash "85678ae90ed2",
                        :name "rabbitmq",
                        :git-commit "9d70b06",
                        :git-branch "develop",
                        :semantic-version "0.1.12-bogus",
                        :build-timestamp #inst "2017-03-31T16:29:35.000-00:00",
                        :build-number 47,
                        :type #:db{:ident :artifact.type/docker}}]]
    (testing "get latest artifacts by branch type")
    (is (= res (branch-type->latest-artifacts (db/latest @test-db)
                                              "develop"
                                              :artifact.type/docker)))))



(deftest artifact-name-branch-test
  (dbt/insert-json! @test-db "artifact" "resources/test-data/artifact.json")
  (let [res '(#:artifact{:short-hash "85678ae90ed2"
                         :name "rabbitmq"
                         :git-commit "9d70b06"
                         :git-branch "develop"
                         :semantic-version "0.1.12-bogus"
                         :build-timestamp #inst "2017-03-31T16:29:35.000-00:00"
                         :build-number 47
                         :type #:db{:ident :artifact.type/docker}})]
    (testing "get latest artifacts by branchname and artifact name")
    (is (= res (name-branch->latest-artifacts (db/latest @test-db)
                                              "rabbitmq"
                                              "develop")))))




(deftest artifact-branch-type-tags-latest-test
  (dbt/insert-json! @test-db "artifact" "resources/test-data/artifact.json")
  (dbt/insert-json! @test-db "transition" "resources/test-data/transition.json")
  ;; uut first
  (is (= (artifact-name-branch-tag-ids->latest-artifact
          (db/latest @test-db) 
          "rabbitmq"
          :artifact.type/docker
          "develop"
          [{:key "stage", :value "regression-pass"}])
         ;; expected
         #:artifact{:short-hash "85678ae90ed2",
                    :name "rabbitmq",
                    :git-commit "9d70b06",
                    :git-branch "develop",
                    :semantic-version "0.1.12-bogus",
                    :build-timestamp #inst "2017-03-31T16:29:35.000-00:00",
                    :build-number 47,
                    :type #:db{:ident :artifact.type/docker}})))

(deftest artifacts-by-name-in-env-test
  (dbt/insert-json! @test-db "artifact" "resources/test-data/artifact.json")
  (dbt/insert-json! @test-db "artifact" "resources/test-data/another-artifact.json")
  (dbt/insert-json! @test-db "artifact" "resources/test-data/dupe-artifact.json")
  (dbt/insert-json! @test-db "transition" "resources/test-data/transition.json")
  (dbt/insert-json! @test-db "environment" "resources/test-data/complex-environment.json")
  (is (= (artifacts-by-name-in-env (db/latest @test-db) "staging-1" "rabbitmq")
         '("22278ae37ed3" "85678ae90ed2"))))


(deftest remove-replace-test
  (is (= (remove-and-replace-env-tx  "staging-1" "rabbitmq" ["2" "3"] "bar")
         '([:db/add
            [:environment/name "staging-1"]
            :environment/artifacts
            [:artifact/short-hash "bar"]]
           [:db/retract
            [:environment/name "staging-1"]
            :environment/artifacts
            [:artifact/short-hash "2"]]
           [:db/retract
            [:environment/name "staging-1"]
            :environment/artifacts
            [:artifact/short-hash "3"]]))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment


  (ulog/catcher
   (run-tests))

  (ulog/catcher
   (dbt/setup-fake-db test-db)
   (dbt/insert-json! @test-db "artifact" "resources/test-data/artifact.json")
   (dbt/insert-json! @test-db "artifact" "resources/test-data/another-artifact.json")
   (dbt/insert-json! @test-db "artifact" "resources/test-data/dupe-artifact.json")
   (dbt/insert-json! @test-db "transition" "resources/test-data/transition.json")
   (dbt/insert-json! @test-db "environment" "resources/test-data/complex-environment.json"))
  
  ;;  (dbt/destroy-fake-db test-db)  


  (require '[clojure.tools.trace])
  (clojure.tools.trace/untrace-ns 'org.restivo.build-manager.db.queries)




  

  )
