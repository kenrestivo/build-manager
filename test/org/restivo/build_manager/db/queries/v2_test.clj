(ns org.restivo.build-manager.db.queries.v2-test
  (:require
   [clojure.test :refer :all]
   [utilza.log :as ulog]
   [datomic.api :as d]
   [org.restivo.build-manager.timestamp :as ts]
   [clojure.walk :as walk]
   [org.restivo.build-manager.db.queries :as q]
   [clojure.edn :as edn]
   [clojure.string :as str]
   [org.restivo.build-manager.model :as m]
   [clojure.java.io :as jio]
   [org.restivo.build-manager.db-test :as dbt]
   [org.restivo.build-manager.db.queries-test :as qt]
   [mount.core :as mount]
   [clojure.test :refer :all]
   [org.restivo.build-manager.db.queries.v2 :refer :all]
   [utilza.repl :as urepl]
   [utilza.misc :as umisc]
   [clojure.spec :as spec]
   [clojure.spec.gen :as gen]
   [datomic.api :as d]
   [cheshire.core :as json]
   [org.restivo.build-manager.util :as u]
   [org.restivo.build-manager.db :as db]
   [byte-streams :as bs]
   [taoensso.timbre :as log]
   ))

(defonce test-db (atom {}))

(defn with-db
  [f] 
  (dbt/with-db test-db f))


(use-fixtures :each with-db)


(deftest read-string-query
  (dbt/insert-json! @test-db "artifact" "resources/test-data/artifact.json")
  (dbt/insert-json! @test-db "transition" "resources/test-data/transition.json")
  (->> (d/q (edn/read-string
             "[:find  ?t
         :in $ ?ts
         :where
         [?t :transition/timestamp ?ts]
         ]")
            (db/latest @test-db)
            (edn/read-string "#inst \"2017-03-31T16:29:23.000-00:00\""))
       ffirst
       boolean
       is))

(deftest freeform-query-test-2
  (dbt/insert-json! @test-db "artifact" "resources/test-data/artifact.json")
  (dbt/insert-json! @test-db "transition" "resources/test-data/transition.json")
  (is (= #{["85678ae90ed2"]}
         (let [db (db/latest @test-db)
               datalog "[:find  ?ah
         :in $ ?ts
         :where
         [?t :transition/timestamp ?ts]
         [?t :transition/artifacts ?as]
         [?as :artifact/short-hash ?ah]
         ]"
               vars ["#inst \"2017-03-31T16:29:23.000-00:00\""]]
           (->> vars
                (map edn/read-string)
                (apply d/q (edn/read-string datalog) db ))))))


(deftest branch-artifacts-test
  (dbt/insert-json! @test-db "artifact" "resources/test-data/artifact.json")
  (dbt/insert-json! @test-db "transition" "resources/test-data/transition.json")
  (is (= '(#:artifact{:short-hash "85678ae90ed2",
                      :name "rabbitmq",
                      :git-commit "9d70b06",
                      :git-branch "develop",
                      :semantic-version "0.1.12-bogus",
                      :build-timestamp #inst "2017-03-31T16:29:35.000-00:00",
                      :build-number 47,
                      :type #:db{:ident :artifact.type/docker}})
         (let [db (db/latest @test-db)
               branch-name "develop"]
           (branch->artifacts db branch-name)))))


(deftest branch-type-artifacts-test
  (dbt/insert-json! @test-db "artifact" "resources/test-data/artifact.json")
  (dbt/insert-json! @test-db "transition" "resources/test-data/transition.json")
  (is (= '(#:artifact{:short-hash "85678ae90ed2",
                      :name "rabbitmq",
                      :git-commit "9d70b06",
                      :git-branch "develop",
                      :semantic-version "0.1.12-bogus",
                      :build-timestamp #inst "2017-03-31T16:29:35.000-00:00",
                      :build-number 47,
                      :type #:db{:ident :artifact.type/docker}})
         (let [db (db/latest @test-db)
               branch-name "develop"
               artifact-type :artifact.type/docker]
           (branch-type->artifacts db branch-name artifact-type)))))



;;;;
(comment

  (ulog/catcher
   (run-tests))
  
  (ulog/spewer
   
   )


  
  


  
  )

