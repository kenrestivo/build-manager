(defproject org.restivo/build-manager "0.1.15"
  :description "Build manager"
  :url "https://gitlab.com/kenrestivo/build-manager"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}

  :dependencies [[org.clojure/clojure "1.9.0-alpha14"]

                 ;; almost always needed
                 [cheshire "5.8.0"]
                 [clj-http "3.7.0" :exclusions [riddley]]
                 [mount "0.1.11"]
                 [clj-time "0.14.2"]
                 [org.clojure/tools.trace "0.7.9"]
                 [com.taoensso/timbre "4.10.0"]
                 [com.fzakaria/slf4j-timbre "0.3.8"]
                 [utilza "0.1.95" :exclusions [org.clojure/clojure]]
                 [org.clojure/tools.reader "1.2.2" :exclusions [org.clojure/clojure]] ;; settle the dep wars
                 
                 ;;[org.clojure/core.async "0.2.374"]
                 [camel-snake-kebab "0.4.0"]
                 [circleci/clj-yaml "0.5.6"]

                 ;; db
                 [com.datomic/datomic-free "0.9.5656"]

                 ;; formats
                 [clojure-csv/clojure-csv "2.0.2"]
                 
                 ;; web
                 [ring/ring-core "1.6.3" ]
                 [aleph "0.4.4"]
                 [yada "1.2.11" :exclusions [ring/ring-core com.github.fge/json-schema-validator]]

                 ;; testing
                 [org.clojure/test.check "0.9.0"] ;; so i can run generators from repl
                 [org.clojure/tools.nrepl "0.2.13"] ;; so that the tests don't fail
                 [ring/ring-mock "0.3.2"]
                 ]
  :aliases {"junit" ["with-profile" "test" "do" "test-out" "junit" "junit.xml"]
            "binary" ["with-profile" "uberjar" "bin"]
            "revision" ["run" "-m" "org.restivo.build-manager.log/build-version"]
            ;; XXX NOTE THE HACK! cloverage is broken due to use of Datomic reader literals like :db/id.
            ;; so "cloverage" was replaced with "test" in the below.
            ;; Once that bug is fixed, change this last "test" back to "cloverage"
            "coverage" ["with-profile" "test" "do" "test"]}
  :main ^:skip-aot org.restivo.build-manager.core
  :jvm-opts ["-Djsse.enableSNIExtension=false" ;; for pulling
             "-XX:-OmitStackTraceInFastThrow" ;; for yada/aleph
             "-Duser.timezone=UTC"]
  :profiles {:dev {:plugins [[lein-ancient "0.6.15"]
                             [lein-licenses "0.2.2"]
                             ]}
             :test {:plugins [[lein-difftest "2.0.0"]
                              [lein-test-out "0.3.1"]
                              [org.clojure/test.check "0.9.0"]
                              [lein-cloverage "1.0.10"]]
                    :test-paths ["test" "src"]}
             :uberjar {:aot :all
                       :bin {:name "build-manager"
                             :bin-path "target/uberjar"}
                       :plugins [[lein-bin "0.3.5"]]
                       :uberjar-name "build-manager.jar"}})
