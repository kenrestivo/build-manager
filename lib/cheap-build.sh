#!/bin/bash -xe
# simple cheap developer build
# TO use: set ARTIFACT=foo-bar or whatever
#        and set BUILD_COMMAND to the command to build anything to be built outside of the docker (i.e. jars)
# then . ../lib/cheap-build.sh to source the file

GIT_COMMIT=`git rev-parse --short HEAD`
GIT_BRANCH=`git rev-parse --abbrev-ref HEAD  | sed 's/\//./g' | tr '[:upper:]' '[:lower:]'`
BUILD_NUMBER=${BUILD_NUMBER:-1}
REV=`echo | lein revision | tail -1`
echo "Revision $REV"

${BUILD_COMMAND}

docker build \
       --build-arg=GIT_COMMIT=$GIT_COMMIT \
       --build-arg=GIT_BRANCH=$GIT_BRANCH \
       --build-arg=BUILD_NUMBER=$BUILD_NUMBER \
       --build-arg REVISION=${REV} \
       --tag=$ARTIFACT:cmt-$GIT_COMMIT \
       --rm=true \
       .

docker tag $ARTIFACT:cmt-$GIT_COMMIT $ARTIFACT:$GIT_BRANCH


echo "built $ARTIFACT:cmt-$GIT_COMMIT $ARTIFACT:$GIT_BRANCH"
