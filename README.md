# build-manager


DB for managing builds, revisions, test status, etc

## Requirements
- Java JVM/JRE version 8 *will not work with java version 9, 10, 11 etc. Only version 8*
- Docker

## Obtaining Image
```

```


## Building from source

1. Install leiningen
   https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein
   This will install the leiningen jar in ~/.m2/ (maven)

2. Obtain source
	$ git checkout  

2. Build the artifact, which will be a "uberjar" with the server compiled binary, and a docker image to run it.
```sh
./build.sh
```


# Testing
Run the tests as junit:
```sh
lein junit
```
This will output test results to build.xml in the project source directory.

To run the tests in more readable format:
```sh
lein difftest
```

## CICD


## Bugs


...
## License

Copyright © 2017-2019 ken restivo
EPL

Includes snippets from "Day of Datomic", Copyright (c) Metadata Partners, LLC. All rights reserved., which is also dual licensed EPL


To find all transitive licenses used by all libraries used by this compiled artifact, in CSV format, type

```sh
lein licenses :csv
```
